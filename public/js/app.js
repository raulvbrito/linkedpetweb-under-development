// To add the marker to the map, call setMap();
marker.setMap(map);

var schedule_id = '', petshop_id = '', duration = '';

function sendContactEmail(){
  var data = $('.contact-form form').serializeArray();
  console.log(data);
  $.ajax({
     url: '/guest/users/contact',
     method: "POST",
     data: data,
     success: function(data){
       console.log(data);
     },
     error: function(data){
       console.log(data);
     }
  });
}

function pushCreateModal(){
  $('.push-create-modal').modal();
}

function petshopSave(){
  var data = $('.petshop-edit-form form').serializeArray();
  console.log(data);
  $.ajax({
     url: '/admin/petshops/update',
     method: "POST",
     data: data,
     success: function(data){
       console.log(data);

     },
     error: function(data){
       console.log(data);
     }
  });
}

function userSave(){
  var data = $('#user-create-form').serializeArray();
  $.ajax({
     url: '/admin/users/store',
     method: "POST",
     data: data,
     success: function(data){
       swal({
         title: 'Cliente Cadastrado',
         text: 'Um novo cliente foi salvo e já pode utilizar o app',
         type: "success",
       },
       function(){ location.reload(); }
       );
     },
     error: function(data){
       console.log(data);
     }
  });
}

function userCreate(){
  $('.user-create-modal').modal();
}

function serviceEditModal(id, name, price, new_price, sale){
  if(sale == 1)
    $('input[type="checkbox"]').parent().removeClass('switch-off').addClass('switch-on');
  else
    $('input[type="checkbox"]').parent().removeClass('switch-on').addClass('switch-off');

  $('.service-edit-modal').modal();
  $('.service-edit-modal #service_id').val(id);
  $('.service-edit-modal input[name="name"]').val(name);
  $('.service-edit-modal input[name="price"]').val(price);
  $('.service-edit-modal input[name="new_price"]').val(new_price);
  $('.service-edit-modal input[type="checkbox"]').val(sale);
}

function serviceRemove(){
  $('.cs-loader').fadeIn();
  $.ajax({
     url: '/admin/services/destroy/'+$('.service-edit-modal #service_id').val(),
     method: "GET",
     success: function(data){
       $('.cs-loader').fadeOut();
       swal({
         html: true,
         title: 'Serviço Removido',
         text: 'O serviço de <b>'+$('.service-edit-modal input[name="name"]').val()+'</b> foi removido com sucesso',
         type: "success",
       },
       function(){ location.reload(); }
       );
     },
     error: function(data){
       $.notify({
         icon: "pe-7s-trash",
         message: '<b>Erro na remoção do serviço</b><br>Só podem ser removidos os serviços que não possuem nenhum agendamento atrelado'
       }, {
           type: 'danger',
           timer: 4000
       });
     }
  });
}

function serviceSave(e){
  var data = $('#service-create-form').serializeArray();

  if($('.service-create-modal input[type="checkbox"]').parent().hasClass('switch-on'))
    data[4].value = '1';
  else
    data[4].value = '0';

  console.log(data);

  $.ajax({
     url: '/admin/services/store',
     method: "POST",
     data: data,
     success: function(data){
       swal({
         title: 'Serviço Cadastrado',
         text: 'Um novo serviço foi salvo e disponibilizado para todos os usuários do LinkedPet',
         type: "success",
       },
       function(){ location.reload(); }
       );
     },
     error: function(data){
       console.log(data);
     }
  });
}

function serviceCreate(){
  $('.service-create-modal').modal();
}

function updateAlert(title, text, successTitle, successText, status, id, petshop_id){
  swal({
    title: title,
    text: text,
    type: "warning",
    showCancelButton: true,
    cancelButtonText: "Cancelar",
    confirmButtonColor: "#fe0103",
    confirmButtonText: "Confirmar",
    closeOnConfirm: false
  },
  function(){
    $('.cs-loader').fadeIn();
    $.ajax({
       url: '/admin/schedules/status/'+status+'/'+id+'/'+petshop_id,
       method: "GET",
       success: function(data){
         $('.cs-loader').fadeOut();
         swal({
           title: successTitle,
           text: successText,
           type: "success",
         },
         function(){ location.reload(); }
         );
       },
       error: function(data){
         $('.cs-loader').fadeOut();
         $.notify({
           icon: "pe-7s-note",
           message: "<b>Não foi possível atualizar o status do agendamento</b><br>Tente novamente mais tarde"
         }, {
             type: 'danger',
             timer: 4000
         });
         console.log(data);
       }
    });
  });
}

function updateDurationModal(id, petshop_id){
  schedule_id = id;
  petshop_id = petshop_id;
  $('.update-duration-modal').modal();
}

function updateDuration(petshop_id){
  duration = $('select.drum').val();

  $.ajax({
     url: '/admin/schedules/duration/'+schedule_id+'/'+duration,
     method: "GET",
     success: function(data){
       $('.update-duration-modal').modal('hide');
       updateServiceStatus('start', schedule_id, petshop_id, true);
     },
     error: function(data){
       console.log(data);
     }
  });
}

function updateServiceStatus(status, id, petshop_id, started){
  var title, text, successTitle, successText;
  switch (status) {
    case 'finish':
      title = "O serviço está de fato finalizado?";
      text = "O cliente será notificado para que possa pegar seu pet de volta!";
      successTitle = "Serviço Finalizado";
      successText = "O cliente foi notificado que o serviço está finalizado!";
      break;
    case 'start':
      title = "O serviço já pode ser iniciado?";
      text = "O cliente será notificado de que o serviço foi iniciado e será finalizado dentro do tempo de duração estipulado!";
      successTitle = "Serviço Iniciado";
      successText = "O cliente foi notificado que o serviço foi iniciado!";
      break;
    case 'aprove':
      title = "O serviço pode ser aprovado?";
      text = "Certifique-se de que será realmente possível a realização do serviço na data e hora estipulada pelo cliente!";
      successTitle = "Serviço Aprovado";
      successText = "O cliente foi notificado que o serviço está aprovado!";
      break;
  }

  if(status == 'start' && !started)
    updateDurationModal(id, petshop_id);
  else
    updateAlert(title, text, successTitle, successText, status, id, petshop_id);
}

function updatePetshopAlert(title, text, successTitle, successText, id){
  swal({
    title: title,
    text: text,
    type: "warning",
    showCancelButton: true,
    cancelButtonText: "Cancelar",
    confirmButtonColor: "#fe0103",
    confirmButtonText: "Confirmar",
    closeOnConfirm: false
  },
  function(){
    $('.cs-loader').fadeIn();
    $.ajax({
       url: '/owner/dashboard/status/'+id,
       method: "GET",
       success: function(data){
         $('.cs-loader').fadeOut();
         swal({
           title: successTitle,
           text: successText,
           type: "success",
         },
         function(){ location.reload(); }
         );
       },
       error: function(data){
         $('.cs-loader').fadeOut();
         $.notify({
           icon: "pe-7s-note",
           message: "<b>Não foi possível aprovar o pet shop</b><br>Tente novamente mais tarde"
         }, {
             type: 'danger',
             timer: 4000
         });
         console.log(data);
       }
    });
  });
}

function updatePetshopStatus(id){
  title = "O pet shop pode ser aprovado?";
  text = "Certifique-se de que todas as informações sobre o mesmo estão corretas!";
  successTitle = "Pet shop Aprovado";
  successText = "O cliente foi notificado que está pronto para usar o LinkedPet!";

  updatePetshopAlert(title, text, successTitle, successText, id);
}

function scheduleCancel(id){
  var petshop_id = $('#petshop_id').val();
  var cancel = $('#justification').val();
  console.log(cancel);
  $('.cs-loader').fadeIn();

  $.ajax({
     url: '/admin/schedules/cancel/'+id+'/'+petshop_id+'/'+cancel,
     method: "GET",
     success: function(data){
       console.log(data);
       $('.cs-loader').fadeOut();
       swal({
         title: 'Agendamento Cancelado',
         text: 'Seu cliente foi notificado de que o agendamento foi cancelado',
         type: "success",
       },
       function(){ location.reload(); }
       );
     },
     error: function(data){
       console.log(data);
     }
  });
}

function scheduleCancelConfirm(id){
  $('.schedule-cancel-modal').modal();
  $('.schedule-cancel-modal #schedule_id').val(id);
  $('.schedule-cancel-modal .btn-danger').attr('onclick', 'scheduleCancel('+id+')');
}

function serviceDetails(id){
  $('.cs-loader').fadeIn();
  $.ajax({
     url: '/admin/schedules/details/'+id,
     method: "GET",
     success: function(data){
       $('.service-details-modal').modal();
       console.log(data);
       $('.service-details-modal .modal-title').html(data[0].service_name);
       $('.service-details-modal p.category span.status-modal').removeClass('Fi Em Na Ag Ca');
       $('.service-details-modal p.category span.status-modal').addClass(data[0].status.substring(0, 2)).html(data[0].status);
       $('.service-details-modal p.category span.status-card').html(' · '+moment(data[0].date).calendar());
       $('.userimage').css('background-image', 'url('+data[0].user_image+')');
       $('.petimage').css('background-image', 'url('+data[0].pet_image+')');
       $('.username').html(data[0].username);
       $('.petname').html(data[0].pet_name);
       $('.breed').html(data[0].breed);
       $('.size').html(data[0].size);
       $('.email').html(data[0].email);
       $('.phone').html(data[0].phone);
       $('.note').html(data[0].note);
       $('.price span').html('R$'+data[0].price.replace('.', ','));

       if(data[0].status == "Cancelado")
        $('.service-details-modal .btn-danger').hide();
       else
         $('.service-details-modal .btn-danger').show();

       $('.service-details-modal .btn-danger').attr('onclick', 'scheduleCancelConfirm('+id+')');
       $('.cs-loader').fadeOut();
     },
     error: function(data){
       console.log(data);
     }
  });
}

function petshopDetails(id){
  $('input').val('');
  $('textarea').val('');

  if(id){
    $('.cs-loader').fadeIn();
    $.ajax({
       url: '/owner/dashboard/details/'+id,
       method: "GET",
       success: function(data){
         $('.petshop-details-modal').modal();
         $('.petshop-details-modal form').attr('action', 'http://linkedpet.com.br/owner/dashboard/update');
         $('.petshop-details-modal button[type="submit"]').html('Atualizar Informações');

         console.log(data);
         $('.petshop-details-modal p.category').html(data[0].name);
         $('.userimage').css('background-image', 'url('+data[0].user_image+')');
         $('.petimage').css('background-image', 'url('+data[0].pet_image+')');
         $('input[name="petshop_id"]').val(data[0].id);
         $('input[name="cnpj"]').val(data[0].cnpj);
         $('input[name="name"]').val(data[0].name);
         $('input[name="phone"]').val(data[0].phone);
         $('input[name="address"]').val(data[0].address);
         $('input[name="number"]').val(data[0].number);
         $('input[name="district"]').val(data[0].district);
         $('input[name="city"]').val(data[0].city);
         $('select[name="state"]').val(data[0].state);
         $('input[name="complement"]').val(data[0].complement);
         $('textarea[name="description"]').val(data[0].description);

         $('.cs-loader').fadeOut();
       },
       error: function(data){
         console.log(data);
       }
    });
  }else{
    $('.petshop-details-modal').modal();
    $('.petshop-details-modal form').attr('action', 'http://linkedpet.com.br/owner/dashboard/store');
    $('.petshop-details-modal button[type="submit"]').html('Salvar');
  }
}

function updateWeekdays(){
  var weekdays = '', firstTime = true;;
  $.each($('.weekdays-box .box.active'), function(index, value){
    if(firstTime){
        weekdays += $(value).find('span').text();
        firstTime = false;
    }else{
        weekdays += '|'+$(value).find('span').text();
    }
  });

  $('#weekdays').val(weekdays);
}

function updateWeekends(){
  var weekends = '', firstTime = true;;
  $.each($('.weekends-box .box.active'), function(index, value){
    if(firstTime){
        weekends += $(value).find('span').text();
        firstTime = false;
    }else{
        weekends += '|'+$(value).find('span').text();
    }
  });

  $('#weekends').val(weekends);
}

function updateWeekdaysHours(){
  if($('#ini-uteis').val())
    var iniUteis = parseInt($('#ini-uteis').val().substring(0,2));

  if($('#fim-uteis').val())
    var fimUteis = parseInt($('#fim-uteis').val().substring(0,2));

  var aux = iniUteis, firstTime = true;
  var weekdaysHours = '';

  for(aux; aux < fimUteis; aux++){
    if(firstTime){
        weekdaysHours += aux;
        firstTime = false;
    }else{
        weekdaysHours += '|'+aux;
    }
  }

  $('#weekdays_hours').val(weekdaysHours);
}

function updateWeekendsHours(){
  if($('#ini-fds').val())
    var iniFds = parseInt($('#ini-fds').val().substring(0,2));

  if($('#fim-fds').val())
    var fimFds = parseInt($('#fim-fds').val().substring(0,2));

  var aux = iniFds, firstTime = true;
  var weekendsHours = '';

  for(aux; aux < fimFds; aux++){
    if(firstTime){
        weekendsHours += aux;
        firstTime = false;
    }else{
        weekendsHours += '|'+aux;
    }
  }

  $('#weekends_hours').val(weekendsHours);
}

function initCalendar(events){
  $.each(events, function(index, value){
    value.start = new Date(value.start);
    value.end = new Date(value.end);
    value.className = value.status.substring(0,2);
  });

  $calendar = $('#fullCalendar');

  var today = new Date();
  var y = today.getFullYear();
  var m = today.getMonth();
  var d = today.getDate();

  var start, end;

  moment().locale("pt-br");

  $('#fullCalendar').fullCalendar({
    lang: 'pt-br',
    header: {
      left: 'title',
      center: 'month,agendaWeek,agendaDay',
      right: 'prev,next today'
    },
    defaultDate: today,
    selectable: true,
    selectHelper: true,
    titleFormat: {
      month: 'MMMM YYYY', // September 2015
      week: "MMMM YYYY", // September 2015
      day: 'D MMM, YYYY'  // Tuesday, Sep 8, 2015
    },
    eventClick: function(event) {
      serviceDetails(event.id);
    },
    select: function(start, end) {
      if(moment().diff(start.add(3, 'hours'), 'days') <= 0 || moment().diff(start.add(3, 'hours'), 'minutes') <= 0){
        $('.datetimepicker').data("DateTimePicker").date(moment(start).subtract(3, 'hours'));
        $('.schedule-add-modal').modal();
        $('#date').val(moment($('.datetimepicker').val()).format());
        userPets();
      }
    },
    eventDrop: function(event){
      start = event.start.format().replace('T', ' ');
      end = event.end.format().replace('T', ' ');

      $.ajax({
         url: '/admin/calendar/time/'+event.id+'/'+start+'/'+end,
         method: "GET",
         success: function(data){
            console.log(data);
         },
         error: function(data){
           console.log(data);
         }
      });
    },
    eventResize: function(event, delta, revertFunc) {
      start = event.start.format().replace('T', ' ');
      end = event.end.format().replace('T', ' ');

      $.ajax({
         url: '/admin/calendar/time/'+event.id+'/'+start+'/'+end,
         method: "GET",
         success: function(data){
            console.log(data);
         },
         error: function(data){
           console.log(data);
         }
      });
    },
    editable: true,
    eventLimit: true,
    events: events
  });
}

function userPets(){
  $('.cs-loader').fadeIn();
  var user_id = $('#user_id').val();
  $.ajax({
     url: '/admin/calendar/pets/'+user_id,
     method: "GET",
     success: function(data){
      $('.pet-grid').empty();
      $('.cs-loader').fadeOut();

      console.log(data);

      var pet;
      $.each(data, function(index, value){
         pet =  '<div class="col-md-3">'
             +  ' <div class="text-center" pet_id="'+value.id+'">'
             +  '  <div class="circle center" style="background-image: url('+value.image+')"></div>'
             +  '  <div class="clearfix"></div>'
             +  '  <h6 class="grayer">'+value.name+'</h6>'
             +  '  <p class="gray">'+value.breed+' · '+value.size+'</p>'
             +  ' </div>'
             +  '</div>';

         $('.pet-grid').append(pet);
      });

      $('.pet-grid .col-md-3 .text-center').unbind('click');
      $('.pet-grid .col-md-3 .text-center').bind('click', function(e){
         $('.pet-grid .col-md-3 .text-center').removeClass('active');
         $(e.currentTarget).addClass('active');
         $('#pet_id').val($(e.currentTarget).attr('pet_id'));
      });
    },
    error: function(data){
      console.log(data);
    }
  });
}

$('.btnNext').click(function(){
  $('.nav-pills > .active').next('li').find('a').trigger('click');
});

$('.btnPrevious').click(function(){
  $('.nav-pills > .active').prev('li').find('a').trigger('click');
});

$(document).ready(function(){
    $('table').DataTable(
      {"language": {
          "sEmptyTable": "Nenhum registro encontrado",
          "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
          "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
          "sInfoFiltered": "(Filtrados de _MAX_ registros)",
          "sInfoPostFix": "",
          "sInfoThousands": ".",
          "sLengthMenu": "_MENU_ resultados por página",
          "sLoadingRecords": "Carregando...",
          "sProcessing": "Processando...",
          "sZeroRecords": "Nenhum registro encontrado",
          "sSearch": "Pesquisar",
          "oPaginate": {
              "sNext": ">",
              "sPrevious": "<",
              "sFirst": "<<",
              "sLast": ">>"
          },
          "oAria": {
              "sSortAscending": ": Ordenar colunas de forma ascendente",
              "sSortDescending": ": Ordenar colunas de forma descendente"
          }
      }}
    );

    // $('select, .dropdown-toggle, .bootstrap-select, .selectpicker').unbind();

    $('.actual-date span').text(moment().format('LLLL'));
    setInterval(function(){
      $('.actual-date span').text(moment().format('LLLL'));
    }, 1000);

    var back_image = document.getElementById('back_image');
    if(back_image)
      back_image.files[0] = $('label[for="back_image"] img').attr('src');

    var image = document.getElementById('image');
    if(image)
      image.files[0] = $('label[for="image"] img').attr('src');

    $('select[name="state"]').val($('select[name="state"]').attr('value'));

    // setTimeout(function(){
    //   var $table = $('#bootstrap-table');
    //
    //   $table.bootstrapTable({
    //       toolbar: ".toolbar",
    //       showRefresh: true,
    //       search: true,
    //       showToggle: true,
    //       showColumns: true,
    //       pagination: true,
    //       searchAlign: 'left',
    //       pageSize: 8,
    //       clickToSelect: false,
    //       pageList: [8,10,25,50,100],
    //
    //       formatShowingRows: function(pageFrom, pageTo, totalRows){
    //           //do nothing here, we don't want to show the text "showing x of y from..."
    //       },
    //       formatRecordsPerPage: function(pageNumber){
    //           return pageNumber + " rows visible";
    //       },
    //       icons: {
    //           refresh: 'fa fa-refresh',
    //           toggle: 'fa fa-th-list',
    //           columns: 'fa fa-columns',
    //           detailOpen: 'fa fa-plus-circle',
    //           detailClose: 'fa fa-minus-circle'
    //       }
    //   });
    // }, 5000);

    $('#image').change(function(){
        var input = document.getElementById('image');
        file = input.files[0];
        fileReader = new FileReader();
        fileReader.readAsDataURL(file);
        fileReader.onload = function(event){
            $('.petshop-image').css('background-image', 'url('+event.target.result+')').attr('src', event.target.result).empty();
        };
    });

    $('#back_image').change(function(){
        var input = document.getElementById('back_image');
        file = input.files[0];
        fileReader = new FileReader();
        fileReader.readAsDataURL(file);
        fileReader.onload = function(event){
            $('.petshop-back-image label[for="back_image"]').html('');
            $('.petshop-back-image').css('background-image', 'url('+event.target.result+')').attr('src', event.target.result);
        };
    });

    $('.add-service').click(function(){
      var servicesContainer = $('.service-container:eq(0)').clone();
      $('.service-container').last().after(servicesContainer);
    });

    $('ul.nav li').removeClass('active');
    $('.nav-pills.petshop-create .bg-darker-gray:first-child').addClass('active');
    $('ul.nav .'+location.pathname.split('/')[2]).addClass('active');

    $('.box span').click(function(){
      $(this).parent().toggleClass('active');
      updateWeekdays();
      updateWeekends();
    });

    $('.btn-finish').click(function(){
      var services = {};
      var service = "";

      $.each($('.service-container'), function(index, value){
        if(index == 0)
          service += '{ "service":'+$(value).find('input[name="service"]').val()+'}';
        else
          service += ',{ "service":'+$(value).find('input[name="service"]').val()+'}';
      });

      services = service;

      console.log(services);
      $('input[name="services"]').val(JSON.stringify(services));
    });

    $('.clockpicker').clockpicker({
      donetext: 'Ok',
      afterHourSelect: function() {
          $('.clockpicker-button').click();
      }
    });

    $('.clockpicker input').change(function(){
      updateWeekdaysHours();
      updateWeekendsHours();
    });

    $.each($('.time-to-finish span'), function(index, value){
      $(value).find('.duration').remove();
      $(value).text(moment($(value).text()).locale("pt-br").fromNow());
    });

    $.each($('.time-to-start span'), function(index, value){
      $(value).text(moment($(value).text()).locale("pt-br").fromNow());
    });

    $.each($('.waiting-time span'), function(index, value){
      $(value).text(moment($(value).text()).locale("pt-br").format('llll'));
    });

    if($('#petshop_id').val())
      var url = '/admin/calendar/events/'+$('#petshop_id').val();
    else
      var url = '/owner/calendar/events/';

    $.ajax({
       url: url,
       method: "GET",
       success: function(data){
         initCalendar(data);
       },
       error: function(data){
         console.log(data);
       }
    });

    moment().locale("pt-br");

    $("select.drum").drum();
    $('.selectpicker').selectpicker();
    $('.datetimepicker').datetimepicker({
        minDate: moment().format(),
        locale: moment.locale('pt-br'),
        sideBySide: true,
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });

    $('.datetimepicker').change(function(){
      $('#date').val($('.datetimepicker').data("DateTimePicker").viewDate().format().replace('T', ' '));
    });

    $('#user_id').change(function(e){
      userPets();
    });

    if($('#weekdays_hours').val()){
      var weekdays_hours_ini = $('#weekdays_hours').val().split('|')[0];
      if(weekdays_hours_ini.length == 1)
        weekdays_hours_ini = '0'+weekdays_hours_ini+':00';
      else
        weekdays_hours_ini = weekdays_hours_ini+':00';

      $('#ini-uteis').val(weekdays_hours_ini);

      var weekdays_hours_fim = $('#weekdays_hours').val().split('|');
      weekdays_hours_fim = weekdays_hours_fim[weekdays_hours_fim.length-1]
      if(weekdays_hours_fim.length == 1)
        weekdays_hours_fim = '0'+parseInt(weekdays_hours_fim)+1+':00';
      else
        weekdays_hours_fim = parseInt(weekdays_hours_fim)+1+':00';

      $('#fim-uteis').val(weekdays_hours_fim);
    }

    if($('#weekends_hours').val()){
      var weekends_hours_ini = $('#weekends_hours').val().split('|')[0];
      if(weekends_hours_ini.length == 1)
        weekends_hours_ini = '0'+weekends_hours_ini+':00';
      else
        weekends_hours_ini = weekends_hours_ini+':00';

      $('#ini-fds').val(weekends_hours_ini);

      var weekends_hours_fim = $('#weekends_hours').val().split('|');
      weekends_hours_fim = weekends_hours_fim[weekends_hours_fim.length-1]
      if(weekends_hours_fim.length == 1)
        weekends_hours_fim = '0'+parseInt(weekends_hours_fim)+1+':00';
      else
        weekends_hours_fim = parseInt(weekends_hours_fim)+1+':00';

      $('#fim-fds').val(weekends_hours_fim);
    }

    if($('#weekdays').val()){
      $.each($('#weekdays').val().split('|'), function(index, value){
        console.log(value);
        $('.'+value).addClass('active');
      });
    }

    if($('#weekends').val()){
      $.each($('#weekends').val().split('|'), function(index, value){
        console.log(value);
        $('.'+value).addClass('active');
      });
    }

    updateWeekdays();
    updateWeekends();
    updateWeekdaysHours();
    updateWeekendsHours();

    $('input[type="checkbox"]').bootstrapSwitch();

    $.each($('.price'), function(index, value){
        $(value).html($(value).html().replace('<span>Preço</span> · R$0.00', '-----'));
        $(value).html($(value).html().replace('.', ','));
    });

    $('#date').val($('.datetimepicker').data("DateTimePicker").viewDate().format().replace('T', ' '));
});

$(document).on("submit", ".ajax-form form", function(event){
    $('.cs-loader').fadeIn();
    event.preventDefault();

    var url = $(this).attr("action"),
        method = $(this).attr("method");

    if(location.pathname.indexOf('petshops/edit') > -1){
        var title = 'Petshop Atualizado',
            text = 'As informações cadastrais do petshop foram atualizadas com sucesso',
            message = '<b>Não foi possível atualizar as informações do petshop</b><br>Verfique o preenchimento de todos os campos';
    }else if(location.pathname.indexOf('services/index') > -1){
        var title = 'Serviço Atualizado',
            text = 'As informações do serviço foram atualizadas com sucesso',
            message = '<b>Não foi possível atualizar as informações do serviço</b><br>Verfique o preenchimento de todos os campos';
    }else if(location.pathname.indexOf('petshops/operation') > -1){
        var title = 'Horários de Funcionamento Atualizados',
            text = 'As informações de horários de funcionamento do petshop foram atualizadas com sucesso',
            message = '<b>Não foi possível atualizar as informações de horários de funcionamento do petshop</b><br>Verfique o preenchimento de todos os campos';
    }else if(location.pathname.indexOf('calendar/index') > -1){
        var title = 'Serviço Agendado',
            text = 'O serviço foi agendado com sucesso',
            message = '<b>Não foi possível agendar o serviço</b><br>Verfique o preenchimento de todos os campos';
    }else if(location.pathname.indexOf('petshops/create') > -1){
        var title = 'Petshop Cadastrado',
            text = 'Assim que o seu petshop for autorizado, você será notificado via email para que possa começar a usar o LinkedPet',
            message = '<b>Não foi possível cadastrar o seu petshop</b><br>Verfique o preenchimento de todos os campos e imagens';
    }else if(location.pathname.indexOf('notifications/index') > -1){
        var title = 'Notificação Enviada',
            text = 'Seu cliente foi notificado',
            message = '<b>Não foi possível enviar a notificação</b><br>Certifique-se de que seu cliente já fez login em seu smartphone e tente novamente mais tarde';
    }

    var data = $('.datetimepicker').val().split('/')[1] + '/' + $('.datetimepicker').val().split('/')[0] + '/' + $('.datetimepicker').val().split('/')[2];
    $(event.currentTarget).find('#date').val(moment(data).format('YYYY-DD-MM HH:mm:ss').replace('T', ' ').split('-03:00')[0]);

    data = $('#date').val().split(' ')[0].split('-')[0] + '-' + $('#date').val().split(' ')[0].split('-')[2] + '-' + $('#date').val().split(' ')[0].split('-')[1] + ' ' + $('#date').val().split(' ')[1];
    $(event.currentTarget).find('#date').val(data);

    var serviceData = $('#service-update-form').serializeArray();
    if(serviceData[5].name == 'sale'){
      if($('.service-edit-modal input[type="checkbox"]').parent().hasClass('switch-on'))
        $('.service-edit-modal .sale').val(1);
      else
        $('.service-edit-modal .sale').val(0);
    }

    $.ajax({
        url: url,
        type: method,
        dataType: "JSON",
        data: new FormData(this),
        processData: false,
        contentType: false,
        success: function (data, status){
          console.log(data);
          $('.cs-loader').fadeOut();

          swal({
            title: title,
            text: text,
            type: "success",
          },
          function(){ location.reload(); }
          );
        },
        error: function (xhr, desc, err){
          console.log(xhr);
          $('.cs-loader').fadeOut();

          $.notify({
            icon: "pe-7s-note",
            message: message
          }, {
              type: 'danger',
              timer: 4000
          });
        }
    });
});
