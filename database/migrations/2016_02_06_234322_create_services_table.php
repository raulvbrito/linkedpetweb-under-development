<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('petshop_id')->unsigned();
            $table->foreign('petshop_id')->references('id')->on('petshops');
            $table->string('name');
            $table->integer('duration');
            $table->decimal('price', 10, 2);
            $table->decimal('new_price', 10, 2);
            $table->integer('sale');
            $table->integer('deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services');
    }
}
