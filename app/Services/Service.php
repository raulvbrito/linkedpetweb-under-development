<?php
namespace App\Services;

use App\Repositories\ServiceRepository;

class Service
{
    private $serviceRepository;

    public function __construct(ServiceRepository $serviceRepository){
        $this->serviceRepository = $serviceRepository;
    }

    public function create(array $data)
    {
        $this->serviceRepository->create($data);

        \DB::commit();
    }
}
