<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Schedule extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'schedules';

    protected $dates = ['date'];

    protected $fillable = [
        'user_id',
        'service_id',
        'note',
        'date'
    ];

    public function schedules(){
        return $this->hasMany(Schedule::class);
    }
}
