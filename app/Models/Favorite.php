<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Favorite extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'favorites';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'user_id',
        'petshop_id'
    ];

    public function favorites(){
        return $this->hasMany(Favorite::class);
    }
}
