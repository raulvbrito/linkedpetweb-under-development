<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Service extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'services';

    protected $fillable = [
        'petshop_id',
        'name',
        'price',
        'new_price',
        'sale'
    ];

    public function services(){
        return $this->hasMany(Service::class);
    }

    public function petshop(){
        return $this->belongsTo(Petshop::class);
    }
}
