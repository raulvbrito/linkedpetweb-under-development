<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Notification extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'notifications';

    // protected $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'user_id',
        'petshop_id',
        'title',
        'message',
        'unread'
    ];

    public function notifications(){
        return $this->hasMany(Notification::class);
    }
}
