<?php

namespace App\Http\Middleware;

use Closure;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class OAuthCheckRole
{
    private $userRepository;

    public function __construct(userRepository, $userRepository){
        $this->userRepository = $userRepository
    }

    public function handle($request, Closure $next, $role)
    {
        $id = Authorizer::getResourceOwnerId();
        $user = $this->userRepository->find($id);

        if($user->role != $role){
            abort(403, 'Access Forbiden')
        }

        return $next($request);
    }
}
