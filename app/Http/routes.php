<?php

//Web Admin

Route::get('/', function () {
    return view('welcome');
});

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);

Route::group(['prefix'=>'guest'], function(){
  Route::group(['prefix'=>'users'], function(){
      Route::post('contact', ['as' => 'guest.users.contact', 'uses' => 'Web\ContactsController@contact']);
  });
});

Route::group(['prefix'=>'owner', 'middleware'=>'auth'], function(){
    Route::group(['prefix'=>'dashboard'], function(){
        Route::get('index', ['as' => 'owner.dashboard.index', 'uses' => 'Owner\DashboardController@index']);
        Route::get('status/{id}', ['as' => 'owner.dashboard.status', 'uses' => 'Owner\DashboardController@status']);
        Route::get('details/{id}', ['as' => 'owner.dashboard.details', 'uses' => 'Owner\DashboardController@details']);
        Route::post('store', ['as' => 'owner.dashboard.index', 'uses' => 'Owner\DashboardController@store']);
        Route::post('update', ['as' => 'owner.dashboard.update', 'uses' => 'Owner\DashboardController@update']);
    });

    Route::group(['prefix'=>'calendar'], function(){
        Route::get('index', ['as' => 'owner.calendar.index', 'uses' => 'Owner\CalendarController@index']);
        Route::get('events', ['as' => 'owner.events.index', 'uses' => 'Owner\CalendarController@events']);
    });
});

Route::group(['prefix'=>'admin', 'middleware'=>'auth'], function(){
    Route::group(['prefix'=>'petshops'], function(){
        Route::get('', ['as' => 'admin.petshops.index', 'uses' => 'Web\PetshopsController@index']);
        Route::get('index', ['as' => 'admin.petshops.index', 'uses' => 'Web\PetshopsController@index']);
        Route::get('create', ['as' => 'admin.petshops.create', 'uses' => 'Web\PetshopsController@create']);
        Route::post('store', ['as' => 'admin.petshops.index', 'uses' => 'Web\PetshopsController@store']);
        Route::get('edit/{id}', ['as' => 'admin.petshops.edit', 'uses' => 'Web\PetshopsController@edit']);
        Route::post('update', ['as' => 'admin.petshops.update', 'uses' => 'Web\PetshopsController@update']);
        Route::get('operation/{id}', ['as' => 'admin.petshops.operation', 'uses' => 'Web\PetshopsController@operation']);
        Route::post('operationUpdate', ['as' => 'admin.petshops.operationUpdate', 'uses' => 'Web\PetshopsController@operationUpdate']);
        Route::get('destroy/{id}', ['as' => 'admin.petshops.destroy', 'uses' => 'Web\PetshopsController@destroy']);
    });

    Route::group(['prefix'=>'dashboard'], function(){
        Route::get('index/{petshop_id}', ['as' => 'admin.dashboard.index', 'uses' => 'Web\DashboardController@index']);
    });

    Route::group(['prefix'=>'schedules'], function(){
        Route::get('status/{status}/{id}/{petshop_id}', ['as' => 'admin.schedules.status', 'uses' => 'Web\SchedulesController@status']);
        Route::get('details/{id}', ['as' => 'admin.schedules.details', 'uses' => 'Web\SchedulesController@details']);
        Route::get('duration/{id}/{duration}', ['as' => 'admin.schedules.duration', 'uses' => 'Web\SchedulesController@duration']);
        Route::get('rating/{id}/{rating}', ['as' => 'admin.schedules.rating', 'uses' => 'Web\SchedulesController@rating']);
        Route::get('cancel/{id}/{petshop_id}/{justification}', ['as' => 'admin.schedules.cancel', 'uses' => 'Web\SchedulesController@cancel']);
    });

    Route::group(['prefix'=>'calendar'], function(){
        Route::get('index/{petshop_id}', ['as' => 'admin.calendar.index', 'uses' => 'Web\CalendarController@index']);
        Route::get('events/{petshop_id}', ['as' => 'admin.calendar.events', 'uses' => 'Web\CalendarController@events']);
        Route::post('add', ['as' => 'admin.calendar.add', 'uses' => 'Web\CalendarController@add']);
        Route::get('time/{id}/{start}/{end}', ['as' => 'admin.calendar.time', 'uses' => 'Web\CalendarController@time']);
        Route::get('pets/{user_id}', ['as' => 'admin.calendar.pets', 'uses' => 'Web\CalendarController@pets']);
    });

    Route::group(['prefix'=>'services'], function(){
        Route::get('index/{id}', ['as' => 'admin.services.index', 'uses' => 'Web\ServicesController@index']);
        Route::get('create', ['as' => 'admin.services.create', 'uses' => 'Web\ServicesController@create']);
        Route::post('store', ['as' => 'admin.services.index', 'uses' => 'Web\ServicesController@store']);
        Route::get('edit/{id}', ['as' => 'admin.services.edit', 'uses' => 'Web\ServicesController@edit']);
        Route::post('update', ['as' => 'admin.services.update', 'uses' => 'Web\ServicesController@update']);
        Route::get('destroy/{id}', ['as' => 'admin.services.destroy', 'uses' => 'Web\ServicesController@destroy']);
    });

    Route::group(['prefix'=>'users'], function(){
        Route::get('index/{id}', ['as' => 'admin.users.index', 'uses' => 'Web\UsersController@index']);
        Route::post('store', ['as' => 'admin.users.index', 'uses' => 'Web\UsersController@store']);
        Route::post('contact', ['as' => 'admin.users.contact', 'uses' => 'Web\UsersController@contact']);
        Route::get('edit/{id}', ['as' => 'admin.services.edit', 'uses' => 'Web\ServicesController@edit']);
        Route::put('update/{id}', ['as' => 'admin.services.update', 'uses' => 'Web\ServicesController@update']);
        Route::get('destroy/{id}', ['as' => 'admin.services.destroy', 'uses' => 'Web\ServicesController@destroy']);
    });

    Route::group(['prefix'=>'notifications'], function(){
        Route::get('index/{id}', ['as' => 'admin.notifications.index', 'uses' => 'Web\NotificationsController@index']);
        Route::post('store', ['as' => 'admin.notifications.index', 'uses' => 'Web\NotificationsController@store']);
    });

    Route::get('logout', ['as' => 'admin.logout', 'uses' => 'Auth\AuthController@logout']);
});

//Mobile APIs

Route::post('oauth2/token', function() {
    return Response::json(Authorizer::issueAccessToken());
});

/**
 * Endpoints
 *
 * index - Listagem de registros
 * store - Armazenamento de registro
 * show - Seleção de registro pelo $id
 * update - Atualização de todo o registro pelo $id
 * edit - Atualização parcial do registro pelo $id
 * destroy - Remoção de registro pelo $id
 */

Route::group(['prefix' => 'api', 'middleware' => 'oauth', 'as' => 'api'], function () {
    Route::group(['prefix' => 'users', 'as' => 'users'], function () {
        Route::resource('user',
          'Api\Users\UsersController', ['except' => ['create', 'edit', 'destroy']]
        );

        Route::resource('facebookUser',
          'Api\Users\FacebookUsersController', ['except' => ['create', 'edit', 'destroy']]
        );

        Route::post('deviceToken', 'Api\Users\UsersController@updateDeviceToken');
    });

    Route::group(['prefix' => 'services', 'as' => 'services'], function () {
        Route::resource('service',
          'Api\Services\ServicesController', ['except' => ['create', 'edit', 'destroy']]
        );
    });

    Route::group(['prefix' => 'pets', 'as' => 'pets'], function () {
        Route::resource('pet',
          'Api\Pets\PetsController', ['except' => ['create', 'edit']]
        );
    });

    Route::group(['prefix' => 'petshops', 'as' => 'petshops'], function () {
        Route::resource('petshop',
          'Api\Petshops\PetshopsController', ['except' => ['create', 'edit', 'destroy']]
        );

        Route::resource('services',
          'Api\Petshops\PetshopsController@services', ['except' => ['create', 'edit', 'destroy']]
        );

        Route::resource('favorite',
          'Api\Petshops\PetshopsController@favorite', ['except' => ['create', 'edit', 'destroy']]
        );
    });

    Route::group(['prefix' => 'schedules', 'as' => 'schedules'], function () {
        Route::resource('schedule',
          'Api\Schedules\SchedulesController', ['except' => ['create', 'edit']]
        );

        Route::post('rating', 'Api\Schedules\SchedulesController@rating');
    });

    Route::group(['prefix' => 'filters', 'as' => 'filters'], function () {
        Route::resource('filter',
          'Api\Filters\FiltersController', ['except' => ['index', 'create', 'edit']]
        );
    });

    Route::group(['prefix' => 'searches', 'as' => 'searches'], function () {
        Route::resource('search',
          'Api\Searches\SearchesController', ['except' => ['index', 'create', 'edit']]
        );
    });

    Route::group(['prefix' => 'notifications', 'as' => 'notifications'], function () {
        Route::resource('notification',
          'Api\Notifications\NotificationsController', ['except' => ['create', 'edit']]
        );
    });

    Route::group(['prefix' => 'uploads', 'as' => 'uploads'], function () {
        Route::resource('image',
          'Api\Uploads\ImagesController', ['except' => ['create', 'edit', 'destroy']]
        );
    });
});
