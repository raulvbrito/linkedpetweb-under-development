<?php

namespace App\Http\Controllers\Api\Filters;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use App\Http\Controllers\Controller;

class FiltersController extends Controller
{
    public function show($filter, Request $request){
        $data = $request->all();
        $user_id = Authorizer::getResourceOwnerId();

        if($filter == 1){
          $response = json_encode(\DB::table('petshops')->select('petshops.id as searchId', 'petshops.name as search')->where('city', 'like', '%' . $data['city'] . '%')->groupBy('petshops.name')->get(), JSON_UNESCAPED_UNICODE);
        }else if($filter == 2){
          $response = json_encode(\DB::table('services')->select('services.name as searchId', 'services.name as search')->where('deleted', 0)->groupBy('services.name')->get(), JSON_UNESCAPED_UNICODE);
        }else if($filter == 3){
          $response = json_encode(\DB::table('services')->select('services.id as searchId', 'services.name as search')->where('sale', '1')->where('deleted', 0)->groupBy('services.name')->get(), JSON_UNESCAPED_UNICODE);
        }else if($filter == 4){
          $response = json_encode(\DB::table('favorites')->select('petshops.id as searchId', 'petshops.name as search')->join('petshops', 'petshops.id', '=', 'favorites.petshop_id')->where('user_id', $user_id)->groupBy('petshops.name')->get(), JSON_UNESCAPED_UNICODE);
        }else if($filter == 7){
          $response = json_encode(\DB::table('schedules')
                                      ->select('rating')
                                      ->join('services', 'services.id', '=', 'schedules.service_id')
                                      ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                                      ->where('petshops.id', $data['city'])->get(), JSON_UNESCAPED_UNICODE);
        }

        return $response;
    }
}
