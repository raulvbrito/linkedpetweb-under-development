<?php

namespace App\Http\Controllers\Api\Petshops;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use App\Http\Controllers\Controller;

class PetshopsController extends Controller
{
    protected $fillable = [
        'name',
        'description',
        'address',
        'number',
        'district',
        'city',
        'state',
        'complement',
        'phone'
    ];

    public function index(){
        return json_encode(\DB::table('pethsops')
                ->get(), JSON_UNESCAPED_UNICODE);
    }

    public function show($id){
        $response = \DB::table('petshops')
                      ->where('id', '=', $id)
                      ->get();

        $user_id = Authorizer::getResourceOwnerId();

        $favorite = json_encode(\DB::table('favorites')
                              ->where('user_id', $user_id)
                              ->where('petshop_id', $id)
                              ->get(), JSON_UNESCAPED_UNICODE);

        if(strlen($favorite) > 2){
          $response[0]->favorite = '';
        }else{
          $response[0]->favorite = '-outline';
        }

        return $response;
    }

    public function services($id){
        return json_encode(\DB::table('services')
                ->select('id', 'name')
                ->where('petshop_id', '=', $id)
                ->get(), JSON_UNESCAPED_UNICODE);
    }

    public function favorite($id){
        $user_id = Authorizer::getResourceOwnerId();

        $favorite = json_encode(\DB::table('favorites')
                              ->where('user_id', $user_id)
                              ->where('petshop_id', $id)
                              ->get(), JSON_UNESCAPED_UNICODE);

        if(strlen($favorite) > 2){
          \DB::table('favorites')->where('user_id', $user_id)->where('petshop_id', $id)->delete();
          return '-outline';
        }else{
          $favorite = new \App\Models\Favorite();

          $favorite->user_id = $user_id;
          $favorite->petshop_id = $id;
          $favorite->save();

          \DB::commit();
          return '';
        }
    }

    public function store(Request $request){
        $user = new \App\Models\Petshop();
        $data = $request->all();

        $user->name = $data['name'];
        $user->description = $data['description'];
        $user->address = $data['address'];
        $user->number = $data['number'];
        $user->district = $data['district'];
        $user->city = $data['city'];
        $user->state = $data['state'];
        $user->complement = $data['complement'];
        $user->phone = $data['phone'];
        $user->save();

        \DB::commit();
    }
}
