<?php

namespace App\Http\Controllers\Api\Schedules;

use App\Http\Requests;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use App\Http\Controllers\Controller;

class SchedulesController extends Controller
{
    protected $dates = ['date'];

    protected $fillable = [
        'user_id',
        'service_id',
        'note',
        'status',
        'date'
    ];

    public function index(){
        Carbon::setLocale('pt_BR');
        $user_id = Authorizer::getResourceOwnerId();

        // dd($user_id);

        $schedules = \DB::table('schedules')
                      ->select(
                          'schedules.id as schedule_id',
                          'schedules.created_at',
                          'services.name as service_name',
                          'status')
                      ->join('services', 'services.id', '=', 'schedules.service_id')
                      ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                      ->where('user_id', '=', $user_id)
                      ->orderBy('date', 'desc')
                      ->get();

        foreach ($schedules as &$schedule) {
            if($schedule->status == 'Aguardando aprovação'){
                $timeout = Carbon::now()->subHours(3)->diffInHours(Carbon::parse($schedule->created_at), false);

                if($timeout <= -1){
                    $date = Carbon::now()->subHours(3)->toDateTimeString();

                    $user = \DB::table('users')
                                  ->select('device_token')
                                  ->where('id', $user_id)
                                  ->get();

                    $justification = "O serviço de {$schedule->service_name} foi cancelado automaticamente por não ter sido aprovado a tempo pelo petshop";

                    $pusher = new \Dmitrovskiy\IonicPush\PushProcessor('6014ec9b', '9719c9607d50f541af9efc6fe6e8b6f990d0b5a0b48ce48a');
                    $pusher->notify([$user[0]->device_token],[
                          'alert' => $justification
                    ]);

                    \DB::table('schedules')
                        ->where('id', $schedule->schedule_id)
                        ->update(['status'=> 'Cancelado', 'justification' => $justification]);

                    \DB::commit();
                }
            }
        }

        return json_encode(\DB::table('schedules')
              ->select(
                  'schedules.id as schedule_id',
                  'schedules.user_id as user_id',
                  'services.name as service_name',
                  'petshops.name as petshop_name',
                  'petshops.image as image',
                  'date',
                  'end',
                  'schedules.duration as duration',
                  'note',
                  'status',
                  'frequency',
                  'price',
                  'new_price',
                  'sale',
                  'address',
                  'number',
                  'district',
                  'city',
                  'state')
              ->join('services', 'services.id', '=', 'schedules.service_id')
              ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
              ->where('user_id', '=', $user_id)
              ->orderBy('date', 'desc')
              ->get(), JSON_UNESCAPED_UNICODE);
    }

    public function show($id){
        $user_id = Authorizer::getResourceOwnerId();

        return json_encode(\DB::table('schedules')
                ->select(
                    'schedules.id as schedule_id',
                    'services.id as service_id',
                    'services.name as service_name',
                    'petshops.id as petshop_id',
                    'petshops.name as petshop_name',
                    'pets.name as pet_name',
                    'pet_id',
                    'price',
                    'new_price',
                    'sale',
                    'date',
                    'schedules.duration as duration',
                    'note',
                    'status',
                    'frequency',
                    'price',
                    'address',
                    'number',
                    'district',
                    'city',
                    'state',
                    'petshops.image',
                    'weekdays',
                    'weekends',
                    'weekdays_hours',
                    'weekends_hours')
                ->join('services', 'services.id', '=', 'schedules.service_id')
                ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                ->join('pets', 'pets.id', '=', 'schedules.pet_id')
                ->where('schedules.id', '=', $id)
                ->get(), JSON_UNESCAPED_UNICODE);
    }

    public function services($id){
        return json_encode(\DB::table('services')
                ->select('id', 'name')
                ->where('petshop_id', '=', $id)
                ->get(), JSON_UNESCAPED_UNICODE);
    }

    public function rating(Request $request){
        $data = $request->all();

        \DB::table('schedules')
            ->where('id', $data['schedule_id'])
            ->update(['rating' => $data['rating']]);

        \DB::commit();

        return "1";
    }

    public function store(Request $request){
        $schedule = new \App\Models\Schedule();
        $data = $request->all();

        $user_id = Authorizer::getResourceOwnerId();

        $schedule->user_id = $user_id;
        $schedule->service_id = $data['service_id'];
        $schedule->pet_id = $data['pet_id'];
        $schedule->note = $data['note'];
        $schedule->status = 'Aguardando aprovação';
        // $schedule->frequency = $data['frequency'];
        $schedule->date = $data['date'];
        $schedule->end = Carbon::parse($data['date'])->addHours(1)->toDateTimeString();
        $schedule->save();

        \DB::commit();

        return $user_id;
    }

    public function destroy($schedule_id){
        \DB::table('schedules')
          ->where('id', $schedule_id)
          ->update(['status' => 'Cancelado', 'justification' => 'Reagendado/Cancelado']);

        \DB::commit();
    }
}
