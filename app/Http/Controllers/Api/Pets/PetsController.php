<?php

namespace App\Http\Controllers\Api\Pets;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use App\Http\Controllers\Controller;

class PetsController extends Controller
{
    protected $fillable = [
        'user_id',
        'name',
        'ration',
        'breed',
        'size',
        'image'
    ];

    public function index(){
        $user_id = Authorizer::getResourceOwnerId();
        return json_encode(\DB::table('pets')
                ->select(
                    'id',
                    'name',
                    'ration',
                    'animal',
                    'breed',
                    'size',
                    'image')
                ->where('user_id', $user_id)
                ->where('deleted', 0)
                ->get(), JSON_UNESCAPED_UNICODE);
    }

    public function show($id){
        return \DB::table('pets')
                ->select(
                    'id',
                    'name',
                    'ration',
                    'animal',
                    'breed',
                    'size',
                    'image')
                ->where('id', '=', $id)
                ->get();
    }

    public function store(Request $request){
        $pet = new \App\Models\Pet();
        $data = $request->all();

        $user_id = Authorizer::getResourceOwnerId();

        $pet->user_id = $user_id;
        $pet->name = $data['name'];
        $pet->ration = $data['ration'];
        $pet->animal = $data['animal'];
        $pet->breed = $data['breed'];
        $pet->size = $data['size'];
        $pet->image = $data['image'];
        $pet->save();

        \DB::commit();
    }

    public function update(Request $request){
        $data = $request->all();

        if(isset($data['image'])){
          \DB::table('pets')
            ->where('id', $data['id'])
            ->update(['image' => $data['image']]);
        }

        \DB::table('pets')
          ->where('id', $data['id'])
          ->update(['name' => $data['name'], 'ration' => $data['ration'], 'animal' => $data['animal'], 'breed' => $data['breed'], 'size' => $data['size']]);

        \DB::commit();
    }

    public function destroy($pet_id){
        \DB::table('pets')
          ->where('id', $pet_id)
          ->update(['deleted' => 1]);

        \DB::commit();
    }
}
