<?php

namespace App\Http\Controllers\Api\Notifications;

use App\Http\Requests;
use App\Repositories\ServiceRepository;
use App\Services\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{
    protected $fillable = [
        'unread'
    ];

    public function index(){
        $user_id = Authorizer::getResourceOwnerId();

        return json_encode(\DB::table('notifications')
                ->select(
                    'petshop_id',
                    'schedule_id',
                    'title',
                    'message',
                    'unread',
                    'notifications.created_at as created_at',
                    'image')
                ->join('petshops', 'petshops.id', '=', 'notifications.petshop_id')
                ->where('user_id', $user_id)
                ->orderBy('created_at', 'desc')
                ->get(), JSON_UNESCAPED_UNICODE);
    }

    public function show($unread){
        $user_id = Authorizer::getResourceOwnerId();

        \DB::table('notifications')
          ->where('user_id', $user_id)
          ->update(['unread' => $unread]);

        \DB::commit();

        return '1';
    }
}
