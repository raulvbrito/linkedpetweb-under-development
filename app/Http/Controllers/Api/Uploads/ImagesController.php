<?php

namespace App\Http\Controllers\Api\Uploads;

use App\Http\Requests;
use Validator;
use Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImagesController extends Controller
{
    public function store(Request $request){
      $data = $request->all();

      $file = array('file' => Input::file('file'));
      $rules = array('file' => 'required');
      $validator = Validator::make($file, $rules);

      // if (!($validator->fails())) {
        $destinationPath = 'mobileImages';
        $extension = Input::file('file')->getClientOriginalExtension();
        $fileName = rand(11111,99999);
        Input::file('file')->move($destinationPath, $fileName);
        return env('BASE_URL').'mobileImages/'.$fileName;
      // }else{
      //   return '1';
      // }
    }
}
