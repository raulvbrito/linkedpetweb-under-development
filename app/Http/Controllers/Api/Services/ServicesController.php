<?php

namespace App\Http\Controllers\Api\Services;

use App\Http\Requests;
use App\Repositories\ServiceRepository;
use App\Services\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use App\Http\Controllers\Controller;

class ServicesController extends Controller
{
    protected $fillable = [
        'petshop_id',
        'name',
        'price'
    ];

    private $serviceRepository;

    private $service;

    public function __construct(ServiceRepository $serviceRepository, Service $service){
        $this->serviceRepository = $serviceRepository;
        $this->service = $service;
    }

    public function index(Request $request){
        $data = $request->all();

        $response = json_encode(\DB::table('services')
                    ->select(
                        'services.id as service_id',
                        'services.name as service_name',
                        'petshops.id as petshop_id',
                        'petshops.name as petshop_name',
                        'address',
                        'number',
                        'district',
                        'city',
                        'state',
                        'image')
                    ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                    ->where('deleted', 0)
                    ->where('city', 'like', '%' . $data['city'] . '%')
                    ->get(), JSON_UNESCAPED_UNICODE);

        return $response;
    }

    public function show($id){
        return \DB::table('services')
                ->select(
                    'services.id as service_id',
                    'services.name as service_name',
                    'petshops.id as petshop_id',
                    'petshops.name as petshop_name',
                    'price',
                    'new_price',
                    'sale',
                    'address',
                    'number',
                    'district',
                    'city',
                    'state',
                    'image',
                    'weekdays',
                    'weekends',
                    'weekdays_hours',
                    'weekends_hours')
                ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                ->where('services.id', '=', $id)
                ->get();
    }

    public function store(Request $request){
        $data = $request->all();

        return $this->service->create($data);
    }
}
