<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    protected $fillable = [
        'name',
        'email',
        'password',
        'role'
    ];

    public function show($email){
        return \DB::table('users')->select('image', 'name')->where('email', '=', $email)->get();
    }

    public function store(Request $request){
        $user = new \App\Models\User();
        $data = $request->all();

        $user->name = $data['name'];
        $user->phone = $data['phone'];
        $user->email = $data['email'];
        $user->password = \Illuminate\Support\Facades\Hash::make($data["password"]);
        $user->image = $data['image'];
        $user->role = $data['role'];
        $user->save();

        \DB::commit();
    }

    public function updateDeviceToken(Request $request){
        $data = $request->all();

        $id = Authorizer::getResourceOwnerId();
        $deviceToken = $data['userId'];

        \DB::table('users')
          ->where('id', $id)
          ->update(['device_token' => $deviceToken]);

        \DB::commit();

        $response = \DB::table('users')
                        ->where('id', $id)
                        ->get();

        return $response;
    }
}
