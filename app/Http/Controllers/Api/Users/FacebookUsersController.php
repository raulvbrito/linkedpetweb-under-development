<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class FacebookUsersController extends Controller
{
    protected $fillable = [
        'facebook_id',
        'name',
        'email',
        'password',
        'image',
        'phone',
        'role'
    ];

    public function show($facebook_id){
        return json_encode(\DB::table('users')
              ->where('facebook_id', '=', $facebook_id)
              ->get(), JSON_UNESCAPED_UNICODE);
    }

    public function store(Request $request){
        $facebookUser = new \App\Models\User();
        $data = $request->all();

        $facebookUser->facebook_id = $data['facebook_id'];
        $facebookUser->name = $data['name'];
        $facebookUser->phone = $data['phone'];
        $facebookUser->email = $data['email'];
        $facebookUser->password = \Illuminate\Support\Facades\Hash::make($data["password"]);
        $facebookUser->facebook_password = $data["password"];
        $facebookUser->image = $data["image"];
        $facebookUser->role = $data['role'];
        $facebookUser->save();

        \DB::commit();
    }
}
