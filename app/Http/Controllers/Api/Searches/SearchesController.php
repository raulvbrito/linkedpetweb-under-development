<?php

namespace App\Http\Controllers\Api\Searches;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use App\Http\Controllers\Controller;

class SearchesController extends Controller
{
    public function show($filter, Request $request){
        $data = $request->all();

        if($filter == 1){
          $response = json_encode(\DB::table('services')
                                    ->select(
                                        'services.id as service_id',
                                        'services.name as service_name',
                                        'petshops.id as petshop_id',
                                        'petshops.name as petshop_name',
                                        'address',
                                        'number',
                                        'district',
                                        'city',
                                        'state',
                                        'image')
                                    ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                                    ->where('petshops.id', $data['search'])
                                    ->where('city', 'like', '%' . $data['city'] . '%')
                                    ->get(), JSON_UNESCAPED_UNICODE);
        }else if($filter == 2){
          $response = json_encode(\DB::table('services')
                                    ->select(
                                        'services.id as service_id',
                                        'services.name as service_name',
                                        'petshops.id as petshop_id',
                                        'petshops.name as petshop_name',
                                        'address',
                                        'number',
                                        'district',
                                        'city',
                                        'state',
                                        'image')
                                    ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                                    ->where('services.name', $data['search'])
                                    ->where('city', 'like', '%' . $data['city'] . '%')
                                    ->get(), JSON_UNESCAPED_UNICODE);
        }else if($filter == 3){
          $response = json_encode(\DB::table('services')
                                    ->select(
                                        'services.id as service_id',
                                        'services.name as service_name',
                                        'petshops.id as petshop_id',
                                        'petshops.name as petshop_name',
                                        'address',
                                        'number',
                                        'district',
                                        'city',
                                        'state',
                                        'image')
                                    ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                                    ->where('services.sale', 1)
                                    ->where('city', 'like', '%' . $data['city'] . '%')
                                    ->get(), JSON_UNESCAPED_UNICODE);
        }else if($filter == 4){
          $response = json_encode(\DB::table('services')
                                    ->select(
                                        'services.id as service_id',
                                        'services.name as service_name',
                                        'petshops.id as petshop_id',
                                        'petshops.name as petshop_name',
                                        'address',
                                        'number',
                                        'district',
                                        'city',
                                        'state',
                                        'image')
                                    ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                                    ->where('petshops.id', $data['search'])
                                    ->where('city', 'like', '%' . $data['city'] . '%')
                                    ->get(), JSON_UNESCAPED_UNICODE);
        }else if($filter == 5){
          switch($data['search']){
            case '1': $min = 0; $max = 15; break;
            case '2': $min = 15; $max = 30; break;
            case '3': $min = 30; $max = 45; break;
            case '4': $min = 45; $max = 60; break;
            case '5': $min = 75; $max = 90; break;
            case '6': $min = 105; $max = 120; break;
            case '7': $min = 135; $max = 150; break;
            case '8': $min = 150; $max = 165; break;
            case '9': $min = 180; $max = 195; break;
            case '10': $min = 210; $max = 225; break;
            case '11': $min = 240; $max = 255; break;
            case '12': $min = 255; $max = 270; break;
          }

          $response = json_encode(\DB::table('services')
                                    ->select(
                                        'services.id as service_id',
                                        'services.name as service_name',
                                        'petshops.id as petshop_id',
                                        'petshops.name as petshop_name',
                                        'address',
                                        'number',
                                        'district',
                                        'city',
                                        'state',
                                        'image')
                                    ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                                    ->where('price', '>', $min)
                                    ->where('price', '<', $max)
                                    ->where('city', 'like', '%' . $data['city'] . '%')
                                    ->get(), JSON_UNESCAPED_UNICODE);
        }else if($filter == 6){
          $response = json_encode(\DB::table('services')
                                    ->select(
                                        'services.id as service_id',
                                        'services.name as service_name',
                                        'petshops.id as petshop_id',
                                        'petshops.name as petshop_name',
                                        'address',
                                        'number',
                                        'district',
                                        'city',
                                        'state',
                                        'image')
                                    ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                                    ->where('city', 'like', '%' . $data['city'] . '%')
                                    ->get(), JSON_UNESCAPED_UNICODE);
        }else if($filter == 7){
          $response = json_encode(\DB::table('schedules')
                                    ->select('rating')
                                    ->where('petshops.id', $data['search'])
                                    ->get(), JSON_UNESCAPED_UNICODE);
        }

        return $response;
    }
}
