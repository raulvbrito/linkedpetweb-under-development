<?php

namespace App\Http\Controllers\Owner;

use App\Http\Requests;
use Validator;
use Input;
use Redirect;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Mail;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){
        $users = \DB::table('users')
                          ->select(
                              'id',
                              'name',
                              'email',
                              'image')
                          ->get();

        $petshops = \DB::table('petshops')
                          ->get();

        $waiting = \DB::table('petshops')
                          ->where('type', 0)
                          ->get();

        return view('owner/dashboard/index', ['waiting'=>$waiting, 'users'=>$users, 'petshops'=>$petshops, 'linkedPet'=>'LINKEDPET', 'title'=>'Dashboard']);
    }

    public function status($id){
        \DB::table('petshops')
            ->where('id', $id)
            ->update(['type' => 1]);

        $user = \DB::table('petshops')
                      ->select(
                          'users.name as name',
                          'users.email as email')
                      ->join('users', 'users.id', '=', 'petshops.owner')
                      ->where('petshops.id', $id)
                      ->get();

        $name = $user[0]->name;
        $email = $user[0]->email;
        $data = array('name'=>$name, 'email'=>$email, 'message'=>'Parabéns! Sua conta do LinkedPet está pronta para ser usada');
        Mail::send('emails.instructions', $data, function ($m) use ($name, $email){
            $m->from('raulvbrito@gmail.com', 'LinkedPet');

            $m->to($email, $name)->subject('Parabéns! Sua conta do LinkedPet está pronta para ser usada');
        });

        \DB::commit();

        return '1';
    }

    public function details($id){
        $petshop_info = \DB::table('petshops')
                        ->where('id', $id)
                        ->get();

        return $petshop_info;
    }

    public function store(Request $request){
      $data = $request->all();

      $petshop = new \App\Models\Petshop();

      $petshop->name = $data['name'];
      $petshop->cnpj = $data['cnpj'];
      $petshop->description = $data['description'];
      $petshop->address = $data['address'];
      $petshop->number = $data['number'];
      $petshop->district = $data['district'];
      $petshop->city = $data['city'];
      $petshop->state = $data['state'];
      $petshop->complement = $data['complement'];
      $petshop->phone = $data['phone'];
      $petshop->status = 0;
      $petshop->weekdays = $data['weekdays'];
      $petshop->weekends = $data['weekends'];
      $petshop->weekdays_hours = $data['weekdays_hours'];
      $petshop->weekends_hours = $data['weekends_hours'];
      $petshop->owner = $data['owner'];
      $petshop->save();

      \DB::commit();

      $petshop = \DB::table('petshops')
                        ->select('id')
                        ->where('owner', '=', $user_id)
                        ->orderBy('id', 'desc')
                        ->get();

      return '1';
    }

    public function update(Request $request){
        $data = $request->all();

        \DB::table('petshops')
          ->where('id', $data['petshop_id'])
          ->update([
            'name' => $data['name'],
            'cnpj' => $data['cnpj'],
            'description' => $data['description'],
            'address' => $data['address'],
            'number' => $data['number'],
            'district' => $data['district'],
            'city' => $data['city'],
            'state' => $data['state'],
            'complement' => $data['complement'],
            'phone' => $data['phone']
          ]);

        \DB::commit();

        $users = \DB::table('users')
                          ->select(
                              'id',
                              'name',
                              'email',
                              'image')
                          ->get();

        $petshops = \DB::table('petshops')
                          ->get();

        $waiting = \DB::table('petshops')
                          ->where('type', 0)
                          ->get();

        return view('owner/dashboard/index', ['waiting'=>$waiting, 'users'=>$users, 'petshops'=>$petshops, 'linkedPet'=>'LINKEDPET', 'title'=>'Dashboard']);
    }
}
