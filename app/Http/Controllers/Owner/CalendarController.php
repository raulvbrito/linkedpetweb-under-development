<?php

namespace App\Http\Controllers\Owner;

use App\Http\Requests;
use Validator;
use Input;
use Redirect;
use Session;
use Carbon\Carbon;
use Dmitrovskiy\IonicPush\PushProcessor as PushProcessor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CalendarController extends Controller
{
    public function index(){
        $services = \DB::table('services')
                          ->select(
                              'id',
                              'name')
                          ->where('deleted', 0)
                          ->groupBy('name')
                          ->get();

        $users = \DB::table('users')
                          ->select(
                              'users.id as id',
                              'users.name as name')
                          ->join('pets', 'pets.user_id', '=', 'users.id')
                          ->where('role', 'user')
                          ->groupBy('name')
                          ->get();

        $petshop_info = \DB::table('petshops')
                          ->select('name', 'image', 'back_image')
                          ->get();

        return view('owner/calendar/index', ['linkedPet'=>'LINKEDPET', 'title'=>'Agenda', 'services'=>$services, 'users'=>$users]);
    }

    public function events(){
        $response = \DB::table('schedules')
                        ->select(
                            'schedules.id as id',
                            'services.id as service_id',
                            'name as title',
                            'status',
                            'date as start',
                            'end')
                        ->join('services', 'services.id', '=', 'schedules.service_id')
                        ->get();

        return $response;
    }

    public function add(Request $request){
        $schedule = new \App\Models\Schedule();
        $data = $request->all();

        $schedule->user_id = $data['user_id'];
        $schedule->service_id = $data['service_id'];
        $schedule->pet_id = $data['pet_id'];
        $schedule->note = $data['note'];
        $schedule->status = 'Na fila';
        $schedule->date = $data['date'];
        $schedule->end = Carbon::parse($data['date'])->addHours(1)->toDateTimeString();
        $schedule->save();

        \DB::commit();

        return '1';
    }

    public function time($id, $start, $end){
        Carbon::setLocale('pt_BR');

        \DB::table('schedules')
            ->where('id', $id)
            ->update(['date' => $start, 'end' => $end]);

        $user = \DB::table('schedules')
                      ->select('device_token')
                      ->join('users', 'users.id', '=', 'schedules.user_id')
                      ->where('schedules.id', $id)
                      ->get();

        $pusher = new \Dmitrovskiy\IonicPush\PushProcessor('6014ec9b', '9719c9607d50f541af9efc6fe6e8b6f990d0b5a0b48ce48a');

        $pusher->notify([$user[0]->device_token],[
              'alert' => "LinkedPet|Seu agendamento teve o horário alterado"
        ]);

        \DB::commit();

        return "1";
    }

    public function pets($user_id){
        $response = \DB::table('pets')
                        ->select(
                            'id',
                            'name',
                            'breed',
                            'size',
                            'image')
                        ->where('user_id', $user_id)
                        ->get();

        return $response;
    }
}
