<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests;
use Validator;
use Input;
use Redirect;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicesController extends Controller
{
    public function index($petshop_id){
        $response = \DB::table('services')
                          ->select(
                              'id',
                              'name',
                              'price',
                              'new_price',
                              'sale')
                          ->where('petshop_id', $petshop_id)
                          ->where('deleted', 0)
                          ->get();

        $petshop_info = \DB::table('petshops')
                          ->select('name', 'image', 'back_image')
                          ->where('id', $petshop_id)
                          ->get();

        return view('admin/services/index', ['linkedPet'=>'LINKEDPET', 'title'=>'Serviços', 'users'=>[], 'services'=>$response, 'petshopId'=>$petshop_id, 'petshopName'=>$petshop_info[0]->name, 'petshopImage'=>$petshop_info[0]->image, 'backImage'=>$petshop_info[0]->back_image]);
    }

    public function update(Request $request){
        $data = $request->all();

        \DB::table('services')
          ->where('id', $data['service_id'])
          ->update([
            'name' => $data['name'],
            'price' => $data['price'],
            'new_price' => $data['new_price'],
            'sale' => $data['sale']
          ]);

        \DB::commit();

        return '1';
    }

    public function store(Request $request){
        $data = $request->all();

        $service = new \App\Models\Service();

        $service->petshop_id = $data['petshop_id'];
        $service->name = $data['name'];
        $service->price = $data['price'];
        $service->new_price = $data['new_price'];
        $service->sale = $data['sale'];
        $service->save();

        \DB::commit();

        return Redirect::to('/admin/services/index/'.$data['petshop_id']);
    }

    public function destroy($service_id){
        \DB::table('services')
          ->where('id', $service_id)
          ->update([
            'deleted' => 1
          ]);

        \DB::commit();

        return '1';
    }
}
