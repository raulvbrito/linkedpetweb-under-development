<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests;
use Validator;
use Input;
use Redirect;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Mail;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function index($petshop_id){
        $response = \DB::table('schedules')
                          ->select(
                              'users.id as id',
                              'users.name as name',
                              'users.email as email',
                              'users.image as image')
                          ->join('services', 'services.id', '=', 'schedules.service_id')
                          ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                          ->join('users', 'users.id', '=', 'schedules.user_id')
                          ->where('role', 'user')
                          ->where('services.petshop_id', $petshop_id)
                          ->groupBy('users.id')
                          ->orderBy('users.name')
                          ->get();

        $petshop_info = \DB::table('petshops')
                          ->select('name', 'image', 'back_image')
                          ->where('id', $petshop_id)
                          ->get();

        return view('admin/users/index', ['linkedPet'=>'LINKEDPET', 'title'=>'Serviços', 'users'=>$response, 'services'=>[], 'petshopId'=>$petshop_id, 'petshopName'=>$petshop_info[0]->name, 'petshopImage'=>$petshop_info[0]->image, 'backImage'=>$petshop_info[0]->back_image]);
    }

    public function store(Request $request){
        $data = $request->all();

        $user = new \App\Models\User();

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = \Illuminate\Support\Facades\Hash::make("linkedpet");
        $user->facebook_password = "linkedpet";
        $user->role = 'user';
        $user->save();

        $last_user = \DB::table('users')
                          ->select('id')
                          ->orderBy('id', 'desc')
                          ->get();

        $pet = new \App\Models\Pet();

        $pet->user_id = $last_user[0]->id;
        $pet->name = $data['pet_name'];
        $pet->ration = $data['ration'];
        $pet->animal = $data['animal'];
        $pet->breed = $data['breed'];
        $pet->size = $data['size'];
        $pet->save();

        $name = $data['name'];
        $email = $data['email'];
        $data = array('name'=>$data['name'], 'email'=>$data['email'], 'message'=>'Sua senha é linkedpet');
        Mail::send('emails.instructions', $data, function ($m) use ($name, $email){
            $m->from('raulvbrito@gmail.com', 'LinkedPet');

            $m->to($email, $name)->subject('Configure sua conta e baixe nosso app!');
        });

        \DB::commit();

        return "1";
    }

    public function contact(Request $request){
        $data = $request->all();

        // $user = new \App\Models\User();
        //
        // $user->name = $data['name'];
        // $user->email = $data['email'];
        // $user->password = \Illuminate\Support\Facades\Hash::make("linkedpet");
        // $user->facebook_password = "linkedpet";
        // $user->role = 'user';
        // $user->save();


        $name = $data['name'];
        $email = $data['email'];
        $mailSubject = $data['subject'];
        $message = $data['message'];
        $data = array('name'=>$data['name'], 'email'=>$data['email'], 'message'=>$message);
        Mail::send('emails.instructions', $data, function ($m) use ($name, $email, $message){
            $m->from('raulvbrito@gmail.com', 'LinkedPet');

            $m->to($email, $name)->subject('Contato');
        });

        \DB::commit();

        return "1";
    }
}
