<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests;
use Validator;
use Input;
use Redirect;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PetshopsController extends Controller
{
    public function index(){
      $user_id = Auth::getUser()->id;

      $response = \DB::table('petshops')
                        ->select(
                            'id',
                            'name',
                            'district',
                            'city',
                            'state',
                            'image',
                            'back_image')
                        ->where('owner', '=', $user_id)
                        ->get();

      if(Auth::user()->role == 'admin'){
        return redirect('owner/dashboard/index');
      }else{
        if(count($response) > 0){
          return view('admin/petshops/index', ['linkedPet'=>'LINKEDPET', 'title'=>'Escolha seu Petshop', 'users'=>[], 'services'=>[], 'petshops'=>$response, 'petshopId'=>'', 'petshopName'=>'', 'petshopImage'=>'']);
        }else{
          return redirect('admin/petshops/create');
        }
      }
    }

    public function edit($id){
        $petshop_info = \DB::table('petshops')
                        ->where('id', $id)
                        ->get();

        return view('admin/petshops/edit', ['linkedPet'=>'LINKEDPET', 'title'=>'Petshop', 'users'=>[], 'services'=>[], 'petshop'=>$petshop_info[0], 'petshopId'=>$petshop_info[0]->id, 'petshopName'=>$petshop_info[0]->name, 'petshopImage'=>$petshop_info[0]->image, 'backImage'=>$petshop_info[0]->back_image]);
    }

    public function update(Request $request){
        $data = $request->all();

        \DB::table('petshops')
          ->where('id', $data['petshop_id'])
          ->update([
            'name' => $data['name'],
            'cnpj' => $data['cnpj'],
            'description' => $data['description'],
            'address' => $data['address'],
            'number' => $data['number'],
            'district' => $data['district'],
            'city' => $data['city'],
            'state' => $data['state'],
            'complement' => $data['complement'],
            'phone' => $data['phone']
          ]);

        $file = array('image' => Input::file('image'));
        $backFile = array('back_image' => Input::file('back_image'));

        $rules = array('image' => 'required',);
        $backImageRules = array('back_image' => 'required',);

        $validator = Validator::make($file, $rules);
        $backImageValidator = Validator::make($backFile, $backImageRules);

        if (!($validator->fails())) {
          $destinationPath = 'logos';
          $extension = Input::file('image')->getClientOriginalExtension();
          $fileName = rand(11111,99999).'.'.$extension;
          Input::file('image')->move($destinationPath, $fileName);
          $petshopImage = env('BASE_URL').'logos/'.$fileName;

          \DB::table('petshops')
            ->where('id', $data['petshop_id'])
            ->update([
              'image' => $petshopImage
            ]);
        }

        if (!($backImageValidator->fails())) {
          $destinationPath = 'backImages';
          $extension = Input::file('back_image')->getClientOriginalExtension();
          $fileName = rand(11111,99999).'.'.$extension;
          Input::file('back_image')->move($destinationPath, $fileName);
          $petshopBackImage = env('BASE_URL').'backImages/'.$fileName;

          \DB::table('petshops')
            ->where('id', $data['petshop_id'])
            ->update([
              'back_image' => $petshopBackImage
            ]);
        }

        \DB::commit();

        return '1';
    }

    public function create(){
        return view('admin/petshops/create', ['linkedPet'=>'LINKEDPET', 'title'=>'Cadastre seu Petshop', 'users'=>[], 'services'=>[], 'petshopId'=>'', 'petshopImage'=>'', 'petshopName'=>'']);
    }

    public function store(Request $request){
      $data = $request->all();

      $file = array('image' => Input::file('image'));
      $backFile = array('back_image' => Input::file('back_image'));

      $rules = array('image' => 'required',);
      $backImageRules = array('back_image' => 'required',);

      $validator = Validator::make($file, $rules);
      $backImageValidator = Validator::make($backFile, $backImageRules);
      if (!($validator->fails()) && !($backImageValidator->fails())) {
          $petshop = new \App\Models\Petshop();

          $destinationPath = 'logos';
          $extension = Input::file('image')->getClientOriginalExtension();
          $fileName = rand(11111,99999).'.'.$extension;
          Input::file('image')->move($destinationPath, $fileName);
          $petshop->image = env('BASE_URL').'logos/'.$fileName;

          $destinationPath = 'backImages';
          $extension = Input::file('back_image')->getClientOriginalExtension();
          $fileName = rand(11111,99999).'.'.$extension;
          Input::file('back_image')->move($destinationPath, $fileName);
          $petshop->back_image = env('BASE_URL').'backImages/'.$fileName;

          Session::flash('success', 'Upload successfully');

          $user_id = Auth::getUser()->id;

          $petshop->name = $data['name'];
          $petshop->cnpj = $data['cnpj'];
          $petshop->description = $data['description'];
          $petshop->address = $data['address'];
          $petshop->number = $data['number'];
          $petshop->district = $data['district'];
          $petshop->city = $data['city'];
          $petshop->state = $data['state'];
          $petshop->complement = $data['complement'];
          $petshop->phone = $data['phone'];
          $petshop->status = 0;
          $petshop->weekdays = $data['weekdays'];
          $petshop->weekends = $data['weekends'];
          $petshop->weekdays_hours = $data['weekdays_hours'];
          $petshop->weekends_hours = $data['weekends_hours'];
          $petshop->owner = $user_id;
          $petshop->save();

          \DB::commit();

          $petshop = \DB::table('petshops')
                            ->select('id')
                            ->where('owner', '=', $user_id)
                            ->orderBy('id', 'desc')
                            ->get();

          return '1';
      }else{
          return 'Erro no upload de imagens';
      }
    }

    public function operation($id){
      $petshop_info = \DB::table('petshops')
                      ->where('id', $id)
                      ->get();

      // dd($petshop_info[0]);
      return view('admin/petshops/operation', ['linkedPet'=>'LINKEDPET', 'title'=>'Funcionamento', 'users'=>[], 'services'=>[], 'petshop'=>$petshop_info[0], 'petshopId'=>$petshop_info[0]->id, 'petshopName'=>$petshop_info[0]->name, 'petshopImage'=>$petshop_info[0]->image, 'backImage'=>$petshop_info[0]->back_image]);
    }

    public function operationUpdate(Request $request){
      $data = $request->all();

      \DB::table('petshops')
        ->where('id', $data['petshop_id'])
        ->update([
          'weekdays' => $data['weekdays'],
          'weekends' => $data['weekends'],
          'weekdays_hours' => $data['weekdays_hours'],
          'weekends_hours' => $data['weekends_hours']
        ]);

      \DB::commit();

      return '1';
    }
}
