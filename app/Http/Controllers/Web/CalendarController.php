<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests;
use Validator;
use Input;
use Redirect;
use Session;
use Carbon\Carbon;
use App\Http\Controllers\Web\DashboardController as DashboardController;
use Dmitrovskiy\IonicPush\PushProcessor as PushProcessor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CalendarController extends Controller
{
    public function index($petshop_id){
        $services = \DB::table('services')
                          ->select(
                              'id',
                              'name')
                          ->where('petshop_id', $petshop_id)
                          ->where('deleted', 0)
                          ->groupBy('name')
                          ->get();

        $users = \DB::table('users')
                          ->select(
                              'users.id as id',
                              'users.name as name')
                          ->join('pets', 'pets.user_id', '=', 'users.id')
                          ->where('role', 'user')
                          ->groupBy('name')
                          ->get();

        $petshop_info = \DB::table('petshops')
                          ->select('name', 'image', 'back_image')
                          ->where('id', $petshop_id)
                          ->get();

        return view('admin/calendar/index', ['linkedPet'=>'LINKEDPET', 'title'=>'Agenda', 'services'=>$services, 'users'=>$users, 'petshopId'=>$petshop_id, 'petshopName'=>$petshop_info[0]->name, 'petshopImage'=>$petshop_info[0]->image, 'backImage'=>$petshop_info[0]->back_image]);
    }

    public function events($petshop_id){
        $response = \DB::table('schedules')
                        ->select(
                            'schedules.id as id',
                            'services.id as service_id',
                            'name as title',
                            'status',
                            'date as start',
                            'end')
                        ->join('services', 'services.id', '=', 'schedules.service_id')
                        ->where('petshop_id', $petshop_id)
                        ->get();

        return $response;
    }

    public function add(Request $request){
        $schedule = new \App\Models\Schedule();
        $data = $request->all();

        $schedule->user_id = $data['user_id'];
        $schedule->service_id = $data['service_id'];
        $schedule->pet_id = $data['pet_id'];
        $schedule->note = $data['note'];
        $schedule->status = 'Na fila';
        $schedule->date = $data['date'];
        $schedule->end = Carbon::parse($data['date'])->addHours(1)->toDateTimeString();
        $schedule->save();

        \DB::commit();

        return '1';
    }

    public function time($id, $start, $end){
        Carbon::setLocale('pt_BR');

        \DB::table('schedules')
            ->where('id', $id)
            ->update(['date' => $start, 'end' => $end]);

        $user = \DB::table('schedules')
                      ->select('device_token')
                      ->join('users', 'users.id', '=', 'schedules.user_id')
                      ->where('schedules.id', $id)
                      ->get();

        $content = array(
          "en" => "Seu agendamento teve o horário alterado"
        );

        $fields = array(
          'app_id' => "d03f289a-eb73-42e2-baec-ee9ee7249264",
          'include_player_ids' => [$user[0]->device_token],
          'data' => array("foo" => "bar"),
          'contents' => $content
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                               'Authorization: Basic NWE2ZjE4YmItNzQzOC00ZTUyLWExY2EtNTI2NjljMTQwYjMx'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        $return["allresponses"] = $response;
        $return = json_encode( $return);

        \DB::commit();

        return "1";
    }

    public function pets($user_id){
        $response = \DB::table('pets')
                        ->select(
                            'id',
                            'name',
                            'breed',
                            'size',
                            'image')
                        ->where('user_id', $user_id)
                        ->get();

        return $response;
    }
}
