<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests;
use Validator;
use Input;
use Redirect;
use Session;
use Carbon\Carbon;
use App\Http\Controllers\Web\DashboardController as DashboardController;
use Dmitrovskiy\IonicPush\PushProcessor as PushProcessor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SchedulesController extends Controller
{
    private $dashboard;
    private $pushProcessor;

    public function __construct(DashboardController $dashboard, PushProcessor $pushProcessor){
      $this->dashboard = $dashboard;
      $this->pushProcessor = $pushProcessor;
    }

    public function status($status, $id, $petshop_id){
        // dd('teste');

        switch ($status) {
          case "finish":
              $status = "Finalizado";
              $notificationTitle = "Serviço Finalizado";
              $notificationText = "Seu pet já está pronto para voltar para casa. Não esqueça de avaliar o Pet Shop tocando aqui!";
              break;
          case "start":
              $status = "Em andamento";
              $service = \DB::table('schedules')
                            ->select('schedules.duration')
                            ->join('services', 'services.id', '=', 'schedules.service_id')
                            ->where('schedules.id', $id)
                            ->get();
              $notificationTitle = "Serviço Iniciado";
              $notificationText = "Em cerca de {$service[0]->duration} horas você será notificado para que possa buscar seu pet";
              break;
          case "aprove":
              $status = "Na fila";
              $notificationTitle = "Serviço Agendado";
              $notificationText = "Você será notificado assim que o serviço for iniciado";
              break;
        }

        if($status == "Finalizado"){
          $frequency = \DB::table('schedules')
                            ->select(
                              'user_id',
                              'service_id',
                              'pet_id',
                              'note',
                              'frequency',
                              'date',
                              'end'
                            )
                            ->where('id', $id)
                            ->get();

          // dd($frequency[0]->frequency);
          if($frequency[0]->frequency == "Semanal"){
            $schedule = new \App\Models\Schedule();

            $schedule->user_id = $frequency[0]->user_id;
            $schedule->service_id = $frequency[0]->service_id;
            $schedule->pet_id = $frequency[0]->pet_id;
            $schedule->note = $frequency[0]->note;
            $schedule->status = 'Aguardando aprovação';
            $schedule->frequency = "Semanal";
            $schedule->date = Carbon::parse($frequency[0]->date)->addWeeks(1)->toDateTimeString();
            $schedule->end = Carbon::parse($frequency[0]->date)->addWeeks(1)->addHours(1)->toDateTimeString();
            $schedule->save();
          }else if($frequency[0]->frequency == "Quinzenal"){
            $schedule = new \App\Models\Schedule();

            $schedule->user_id = $frequency[0]->user_id;
            $schedule->service_id = $frequency[0]->service_id;
            $schedule->pet_id = $frequency[0]->pet_id;
            $schedule->note = $frequency[0]->note;
            $schedule->status = 'Aguardando aprovação';
            $schedule->frequency = "Mensal";
            $schedule->date = Carbon::parse($frequency[0]->date)->addWeeks(2)->toDateTimeString();
            $schedule->end = Carbon::parse($frequency[0]->date)->addWeeks(2)->addHours(1)->toDateTimeString();
            $schedule->save();

            \DB::commit();
          }
        }

        $user = \DB::table('schedules')
                      ->select('users.id as user_id', 'device_token')
                      ->join('users', 'users.id', '=', 'schedules.user_id')
                      ->where('schedules.id', $id)
                      ->get();

        $reg_id = $user[0]->device_token;

        $title = $notificationTitle;
        $message = $notificationText;

        $notification = new \App\Models\Notification();

        $notification->user_id = $user[0]->user_id;
        $notification->petshop_id = $petshop_id;
        $notification->schedule_id = $id;
        $notification->title = $title;
        $notification->message = $message;
        $notification->save();

        $content = array(
          "en" => $title
        );

        $fields = array(
          'app_id' => "d03f289a-eb73-42e2-baec-ee9ee7249264",
          'include_player_ids' => [$user[0]->device_token],
          'data' => array("foo" => "bar"),
          'contents' => $content
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                               'Authorization: Basic NWE2ZjE4YmItNzQzOC00ZTUyLWExY2EtNTI2NjljMTQwYjMx'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        $return["allresponses"] = $response;
        $return = json_encode( $return);

        \DB::table('schedules')
            ->where('id', $id)
            ->update(['status' => $status]);

        \DB::commit();

        return $this->dashboard->index($petshop_id);
    }

    public function details($id){
        $response = \DB::table('schedules')
                          ->select(
                              'schedules.id as schedule_id',
                              'users.name as username',
                              'users.email as email',
                              'users.phone as phone',
                              'services.name as service_name',
                              'schedules.duration as duration',
                              'note',
                              'status',
                              'date',
                              'end',
                              'price',
                              'pets.image as pet_image',
                              'pets.name as pet_name',
                              'breed',
                              'size',
                              'users.image as user_image')
                          ->join('users', 'users.id', '=', 'schedules.user_id')
                          ->join('services', 'services.id', '=', 'schedules.service_id')
                          ->join('pets', 'pets.id', '=', 'schedules.pet_id')
                          ->where('schedules.id', $id)
                          ->get();
        return $response;
    }

    public function duration($id, $duration){
        Carbon::setLocale('pt_BR');
        $date = Carbon::now()->subHours(3)->toDateTimeString();
        $end = Carbon::now()->addHours($duration)->subHours(3)->toDateTimeString();

        \DB::table('schedules')
            ->where('id', $id)
            ->update(['duration' => $duration, 'date' => $date, 'end' => $end]);

        \DB::commit();

        return "1";
    }

    public function rating($id, $rating){
        \DB::table('schedules')
            ->where('id', $id)
            ->update(['rating' => $rating]);

        \DB::commit();

        return "1";
    }

    public function cancel($id, $petshop_id, $justification){
        Carbon::setLocale('pt_BR');
        $date = Carbon::now()->subHours(3)->toDateTimeString();

        $user = \DB::table('schedules')
                      ->select('device_token')
                      ->join('users', 'users.id', '=', 'schedules.user_id')
                      ->where('schedules.id', $id)
                      ->get();

        $notificationTitle = $justification;
        $notificationText = $justification;

        $deviceToken = $user[0]->device_token;

        $content = array(
          "en" => $notificationText
        );

        $fields = array(
          'app_id' => "d03f289a-eb73-42e2-baec-ee9ee7249264",
          'include_player_ids' => [$deviceToken],
          'data' => array("foo" => "bar"),
          'contents' => $content,
          'ios_badgeType' => "Increase",
          'ios_badgeCount' => 1
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                               'Authorization: Basic NWE2ZjE4YmItNzQzOC00ZTUyLWExY2EtNTI2NjljMTQwYjMx'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        $return["allresponses"] = $response;
        $return = json_encode( $return);

        \DB::table('schedules')
            ->where('id', $id)
            ->update(['status'=> 'Cancelado', 'justification' => $justification, 'date' => $date]);

        \DB::commit();

        return $user[0]->device_token;
    }
}
