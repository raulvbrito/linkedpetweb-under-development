<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests;
use Validator;
use Input;
use Redirect;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Mail;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{
    public $message;

    public function contact(Request $request){
        $data = $request->all();

        // $user = new \App\Models\User();
        //
        // $user->name = $data['name'];
        // $user->email = $data['email'];
        // $user->password = \Illuminate\Support\Facades\Hash::make("linkedpet");
        // $user->facebook_password = "linkedpet";
        // $user->role = 'user';
        // $user->save();


        $name = $data['name'];
        $email = 'contato@linkedpet.com.br';
        $emailFrom = $data['email'];
        $mailSubject = $data['subject'];
        $message = $data['message'];
        $this->message = $data;
        $data = array('name'=>$data['name'], 'email'=>'contato@linkedpet.com.br', 'message'=>$message);
        Mail::send([], $data, function ($m) use ($name, $email, $message, $emailFrom, $mailSubject){
            $m->from($emailFrom, $name);

            $m->to($email, $name)->subject($mailSubject)->setBody($message);
        });

        \DB::commit();

        return "1";
    }
}
