<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests;
use Validator;
use Input;
use Redirect;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Dmitrovskiy\IonicPush\PushProcessor as PushProcessor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{
    public function index($petshop_id){
        Carbon::setLocale('pt_BR');
        setlocale(LC_TIME, 'pt_BR');

        $response = \DB::table('notifications')
                        ->select(
                            'title',
                            'message',
                            'notifications.created_at as created_at',
                            'name',
                            'email',
                            'image')
                        ->join('users', 'users.id', '=', 'notifications.user_id')
                        ->where('petshop_id', $petshop_id)
                        ->where('role', 'user')
                        ->get();

        $users = \DB::table('users')
                          ->select(
                              'users.id as id',
                              'users.name as name')
                          ->join('pets', 'pets.user_id', '=', 'users.id')
                          ->where('role', 'user')
                          ->groupBy('users.name')
                          ->get();

        $petshop_info = \DB::table('petshops')
                          ->select('name', 'image', 'back_image')
                          ->where('id', $petshop_id)
                          ->get();

        return view('admin/notifications/index', ['linkedPet'=>'LINKEDPET', 'title'=>'Serviços', 'notifications'=>$response, 'users'=>$users, 'services'=>[], 'petshopId'=>$petshop_id, 'petshopName'=>$petshop_info[0]->name, 'petshopImage'=>$petshop_info[0]->image, 'backImage'=>$petshop_info[0]->back_image]);
    }

    public function store(Request $request){
        $data = $request->all();

        $notification = new \App\Models\Notification();

        $notification->user_id = $data['user_id'];
        $notification->petshop_id = $data['petshop_id'];
        $notification->title = $data['title'];
        $notification->message = $data['message'];
        $notification->save();

        $user = \DB::table('users')
                      ->select('device_token')
                      ->where('id', $data['user_id'])
                      ->get();

        $notificationTitle = $data['title'];
        $notificationText = $data['message'];

        $deviceToken = $user[0]->device_token;

        $content = array(
          "en" => $notificationText
        );

        $fields = array(
          'app_id' => "d03f289a-eb73-42e2-baec-ee9ee7249264",
          'include_player_ids' => [$deviceToken],
          'data' => array("foo" => "bar"),
          'contents' => $content,
          'ios_badgeType' => "Increase",
          'ios_badgeCount' => 1
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                               'Authorization: Basic NWE2ZjE4YmItNzQzOC00ZTUyLWExY2EtNTI2NjljMTQwYjMx'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        $return["allresponses"] = $response;
        $return = json_encode( $return);

        \DB::commit();

        return '1';
    }
}
