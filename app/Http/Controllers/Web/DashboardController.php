<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests;
use Validator;
use Input;
use Redirect;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index($petshop_id){
        $schedules = \DB::table('schedules')
                      ->select(
                          'schedules.id as schedule_id',
                          'schedules.created_at',
                          'services.name as service_name',
                          'users.id as user_id',
                          'schedules.status as status')
                      ->join('services', 'services.id', '=', 'schedules.service_id')
                      ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                      ->join('users', 'users.id', '=', 'schedules.user_id')
                      ->orderBy('date', 'desc')
                      ->get();

        foreach ($schedules as &$schedule) {
            if($schedule->status == 'Aguardando aprovação'){
                $timeout = Carbon::now()->subHours(3)->diffInHours(Carbon::parse($schedule->created_at)->subHours(3), false);

                if($timeout <= -2){
                    $date = Carbon::now()->subHours(3)->toDateTimeString();

                    $user = \DB::table('users')
                                  ->select('device_token')
                                  ->where('id', $schedule->user_id)
                                  ->get();

                    $justification = "O serviço de {$schedule->service_name} foi cancelado automaticamente por não ter sido aprovado a tempo pelo petshop";

                    $content = array(
                      "en" => $justification
                    );

                    $fields = array(
                      'app_id' => "d03f289a-eb73-42e2-baec-ee9ee7249264",
                      'include_player_ids' => [$user[0]->device_token],
                      'data' => array("foo" => "bar"),
                      'contents' => $content
                    );

                    $fields = json_encode($fields);

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                           'Authorization: Basic NWE2ZjE4YmItNzQzOC00ZTUyLWExY2EtNTI2NjljMTQwYjMx'));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    curl_setopt($ch, CURLOPT_HEADER, FALSE);
                    curl_setopt($ch, CURLOPT_POST, TRUE);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                    $response = curl_exec($ch);
                    curl_close($ch);

                    // $return["allresponses"] = $response;
                    // $return = json_encode($return);

                    \DB::table('schedules')
                        ->where('id', $schedule->schedule_id)
                        ->update(['status'=> 'Cancelado', 'justification' => $justification, 'date' => $date]);

                    \DB::commit();
                }
            }
        }

        $user_id = Auth::getUser()->id;

        $finished = \DB::table('schedules')
                        ->select(
                            'schedules.id as schedule_id',
                            'users.name as username',
                            'users.email as email',
                            'services.name as service_name',
                            'schedules.duration as duration',
                            'note',
                            'status',
                            'date',
                            'pets.image as pet_image',
                            'pets.name as pet_name',
                            'users.image as user_image')
                        ->join('users', 'users.id', '=', 'schedules.user_id')
                        ->join('services', 'services.id', '=', 'schedules.service_id')
                        ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                        ->join('pets', 'pets.id', '=', 'schedules.pet_id')
                        ->where('petshop_id', $petshop_id)
                        ->where('owner', $user_id)
                        ->where('status', 'Finalizado')
                        ->whereRaw('date(date) = ?', [Carbon::now()->subHours(3)->format('Y-m-d')])
                        ->get();

        $ongoins = \DB::table('schedules')
                          ->select(
                              'schedules.id as schedule_id',
                              'users.name as username',
                              'users.email as email',
                              'services.name as service_name',
                              'schedules.duration as duration',
                              'note',
                              'status',
                              'date',
                              'end',
                              'pets.image as pet_image',
                              'pets.name as pet_name',
                              'users.image as user_image')
                          ->join('users', 'users.id', '=', 'schedules.user_id')
                          ->join('services', 'services.id', '=', 'schedules.service_id')
                          ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                          ->join('pets', 'pets.id', '=', 'schedules.pet_id')
                          ->where('petshop_id', $petshop_id)
                          ->where('owner', $user_id)
                          ->where('status', 'Em andamento')
                          ->get();

        $nextrow = \DB::table('schedules')
                          ->select(
                              'schedules.id as schedule_id',
                              'users.name as username',
                              'users.email as email',
                              'services.name as service_name',
                              'schedules.duration',
                              'note',
                              'status',
                              'date',
                              'pets.image as pet_image',
                              'pets.name as pet_name',
                              'users.image as user_image')
                          ->join('users', 'users.id', '=', 'schedules.user_id')
                          ->join('services', 'services.id', '=', 'schedules.service_id')
                          ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                          ->join('pets', 'pets.id', '=', 'schedules.pet_id')
                          ->where('petshop_id', $petshop_id)
                          ->where('owner', $user_id)
                          ->where('status', 'Na fila')
                          ->whereRaw('date(date) = ?', [Carbon::now()->subHours(3)->format('Y-m-d')])
                          ->get();

        $waiting = \DB::table('schedules')
                          ->select(
                              'schedules.id as schedule_id',
                              'users.name as username',
                              'users.email as email',
                              'services.name as service_name',
                              'schedules.duration',
                              'note',
                              'status',
                              'date',
                              'pets.image as pet_image',
                              'pets.name as pet_name',
                              'users.image as user_image')
                          ->join('users', 'users.id', '=', 'schedules.user_id')
                          ->join('services', 'services.id', '=', 'schedules.service_id')
                          ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                          ->join('pets', 'pets.id', '=', 'schedules.pet_id')
                          ->where('petshop_id', $petshop_id)
                          ->where('owner', $user_id)
                          ->where('status', 'Aguardando aprovação')
                          ->get();

        $canceled = \DB::table('schedules')
                          ->select(
                              'schedules.id as schedule_id',
                              'users.name as username',
                              'users.email as email',
                              'services.name as service_name',
                              'schedules.duration',
                              'note',
                              'status',
                              'date',
                              'pets.image as pet_image',
                              'pets.name as pet_name',
                              'users.image as user_image')
                          ->join('users', 'users.id', '=', 'schedules.user_id')
                          ->join('services', 'services.id', '=', 'schedules.service_id')
                          ->join('petshops', 'petshops.id', '=', 'services.petshop_id')
                          ->join('pets', 'pets.id', '=', 'schedules.pet_id')
                          ->where('petshop_id', $petshop_id)
                          ->where('owner', $user_id)
                          ->where('status', 'Cancelado')
                          ->whereRaw('date(date) = ?', [Carbon::now()->subHours(3)->format('Y-m-d')])
                          ->get();

        $petshop_info = \DB::table('petshops')
                          ->select('name', 'image', 'back_image')
                          ->where('id', $petshop_id)
                          ->where('owner', $user_id)
                          ->get();

        return view('admin/dashboard/index', ['linkedPet'=>'LINKEDPET', 'title'=>'Dashboard', 'users'=>[], 'services'=>[], 'petshopId'=>$petshop_id, 'finished'=>$finished, 'ongoings'=>$ongoins, 'nextrow'=>$nextrow, 'waiting'=>$waiting, 'canceled'=>$canceled, 'petshopName'=>$petshop_info[0]->name, 'petshopImage'=>$petshop_info[0]->image, 'backImage'=>$petshop_info[0]->back_image]);
    }
}
