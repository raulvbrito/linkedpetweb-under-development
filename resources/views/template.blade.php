<!doctype html>
<html lang="pt-br">
<head>
		<meta charset="utf-8" />
		<link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<title>LinkedPet</title>

		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('css/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet" />
		<link href="{{ asset('css/fullcalendar.css') }}" rel="stylesheet" />
		<link href="{{ asset('css/drum.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="{{ asset('css/light-bootstrap-dashboard.css') }}" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/pe-icon-7-stroke.css') }}" rel="stylesheet" />

		<!--  	 Custom Styles				-->
		<link href="{{ asset('css/estrutura.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" />
</head>
<body>

<div class="wrapper">
    <div class="sidebar bg-second-color" data-color="orange">
    	<div class="sidebar-wrapper">
            <div class="logo">
								<a class="navbar-brand logo" href="http://www.linkedpet.com.br"></a>
								<div class="clearfix"></div>
            </div>
						<div class="user">
                <div class="photo" style="background-image: url({{ $petshopImage }})"></div>
                <div class="info text-center">
                    <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                        {{ $petshopName }}
                        <b class="caret"></b>
                    </a>
                    <div class="collapse" id="collapseExample">
                        <ul class="nav">
                            <li><a href="/admin/petshops/operation/{{ $petshopId }}">Funcionamento</a></li>
														<li><a href="/admin/petshops/edit/{{ $petshopId }}">Editar Informações</a></li>
														<li><a href="/admin/petshops/create">Adicionar Pet shop</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav">
                <li class="dashboard">
                    <a href="/admin/dashboard/index/{{ $petshopId }}">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
								<li class="calendar">
                    <a href="/admin/calendar/index/{{ $petshopId }}">
                        <i class="pe-7s-date"></i>
                        <p>Agenda</p>
                    </a>
                </li>
								<li class="services">
                    <a href="/admin/services/index/{{ $petshopId }}">
                        <i class="pe-7s-plugin"></i>
                        <p>Serviços</p>
                    </a>
                </li>
								<li class="users">
                    <a href="/admin/users/index/{{ $petshopId }}">
                        <i class="pe-7s-users"></i>
                        <p>Usuários</p>
                    </a>
                </li>
								<li class="notifications">
                    <a href="/admin/notifications/index/{{ $petshopId }}">
                        <i class="pe-7s-speaker"></i>
                        <p>Notificações</p>
                    </a>
                </li>
            </ul>
						<ul class="nav logout-btn">
                <li class="dashboard">
                    <a href="/admin/logout">
                        <i class="pe-7s-power"></i>
                        <p>Logout</p>
                    </a>
                </li>
						</ul>
    	</div>
    </div>

		<div class="cs-loader">
			<div class="cs-loader-inner">
				<label></label>
				<label></label>
				<label></label>
				<label></label>
				<label></label>
				<label></label>
			</div>
		</div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><span>Admin</span> / {{ $title }}</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- <i class="fa fa-dashboard"></i> -->
                            </a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="actual-date">
													<span></span>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
						@yield('content')
        </div>


        <!-- <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="#">
																{{ $linkedPet }}
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </footer> -->

    </div>
</div>
<div class="modal fade user-create-modal" tabindex="-1" role="dialog" style="padding-top: 15rem">
  <div class="modal-dialog modal-sm" style="width: 400px;">
    <div class="modal-content">
			{!! Form::open(array('url'=>'admin/users/store','method'=>'POST', 'id'=>'user-create-form')) !!}
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Cadastrar Cliente</h4>
	      </div>
	      <div class="modal-body">
					<input type="hidden" id="petshop_id" name="petshop_id" value="{{ $petshopId }}">
					<div class="col-md-6">
							<div class="form-group">
									<label class="control-label">Nome</label>
									<input class="form-control" type="text" id="name" name="name" placeholder="Ex: João da Silva">
							</div>
					</div>
					<div class="col-md-6">
							<div class="form-group">
									<label class="control-label">Email</label>
									<input class="form-control" type="email" id="email" name="email" placeholder="Ex: joao.silva@gmail.com">
							</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-6">
							<div class="form-group">
									<label class="control-label">Nome do Pet</label>
									<input class="form-control" type="text" id="pet_name" name="pet_name" placeholder="Ex: Dino">
							</div>
					</div>
					<div class="col-md-6">
							<div class="form-group">
									<label class="control-label">Tipo do Animal</label>
									<select name="animal" id="animal" class="selectpicker" data-style="btn-default btn-block">
										<option value="Cão">Cão</option>
										<option value="Gato">Gato</option>
										<option value="Outro">Outro</option>
									</select>
							</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12">
							<div class="form-group">
									<label class="control-label">Raça</label>
									<select name="breed" id="breed" class="selectpicker" data-style="btn-default btn-block" data-live-search="true">
										<optgroup label="Cão">
											<option value="Affenpinscher">Affenpinscher</option>
											<option value="Afghan Hound">Afghan Hound</option>
											<option value="Airedale Terrier">Airedale Terrier</option>
											<option value="Akita">Akita</option>
											<option value="Akita Americano">Akita Americano</option>
											<option value="American Pit Bull Terrier">American Pit Bull Terrier</option>
											<option value="American Staffordshire Terrier">American Staffordshire Terrier</option>
											<option value="Australian Shepherd">Australian Shepherd</option>
											<option value="Basenji">Basenji</option>
											<option value="Basset Fulvo da Bretanha">Basset Fulvo da Bretanha</option>
											<option value="Basset Hound">Basset Hound</option>
											<option value="Beagle">Beagle</option>
											<option value="Beagle­Harrier">Beagle­Harrier</option>
											<option value="Bearded Collie">Bearded Collie</option>
											<option value="Bedlington Terrier">Bedlington Terrier</option>
											<option value="Bernese Mountain Dog">Bernese Mountain Dog</option>
											<option value="Bichon Bolonhês">Bichon Bolonhês</option>
											<option value="Bichon Frisé">Bichon Frisé</option>
											<option value="Bichon Havanês">Bichon Havanês</option>
											<option value="Boerboel">Boerboel</option>
											<option value="Boiadeiro de Entlebuch">Boiadeiro de Entlebuch</option>
											<option value="Border Collie">Border Collie</option>
											<option value="Border Terrier">Border Terrier</option>
											<option value="Borzoi">Borzoi</option>
											<option value="Boston Terrier">Boston Terrier</option>
											<option value="Bouvier de Flandres">Bouvier de Flandres</option>
											<option value="Boxer">Boxer</option>
											<option value="Braco Alemão Pelo Curto">Braco Alemão Pelo Curto</option>
											<option value="Braco Alemão Pelo Duro">Braco Alemão Pelo Duro</option>
											<option value="Braco Italiano">Braco Italiano</option>
											<option value="Buldogue Americano">Buldogue Americano</option>
											<option value="Buldogue Francês">Buldogue Francês</option>
											<option value="Buldogue Inglês">Buldogue Inglês</option>
											<option value="Bull Terrier">Bull Terrier</option>
											<option value="Bullmastiff">Bullmastiff</option>
											<option value="Cairn Terrier">Cairn Terrier</option>
											<option value="Cane Corso">Cane Corso</option>
											<option value="Cão de Crista Chinês">Cão de Crista Chinês</option>
											<option value="Cão de Santo Humberto">Cão de Santo Humberto</option>
											<option value="Cão D’água Espanhol">Cão D’água Espanhol</option>
											<option value="Cão D’água Português">Cão D’água Português</option>
											<option value="Cão Lobo Checoslovaco">Cão Lobo Checoslovaco</option>
											<option value="Cão Pelado Mexicano">Cão Pelado Mexicano</option>
											<option value="Cão Pelado Peruano">Cão Pelado Peruano</option>
											<option value="Cavalier King Charles Spaniel">Cavalier King Charles Spaniel</option>
											<option value="Cesky Terrier">Cesky Terrier</option>
											<option value="Chesapeake Bay Retriever">Chesapeake Bay Retriever</option>
											<option value="Chihuahua">Chihuahua</option>
											<option value="Chin">Chin</option>
											<option value="Chow­chow Pelo Curto">Chow­chow Pelo Curto</option>
											<option value="Chow­chow Pelo Longo">Chow­chow Pelo Longo</option>
											<option value="Cirneco do Etna">Cirneco do Etna</option>
											<option value="Clumber Spaniel">Clumber Spaniel</option>
											<option value="Cocker Spaniel Americano">Cocker Spaniel Americano</option>
											<option value="Cocker Spaniel Inglês">Cocker Spaniel Inglês</option>
											<option value="Collie pelo longo">Collie pelo longo</option>
											<option value="Coton de Tulear">Coton de Tulear</option>
											<option value="Dachshund Teckel ­ Pelo Curto">Dachshund Teckel ­ Pelo Curto</option>
											<option value="Dálmata">Dálmata</option>
											<option value="Dandie Dinmont Terrier">Dandie Dinmont Terrier</option>
											<option value="Dobermann">Dobermann</option>
											<option value="Dogo Argentino">Dogo Argentino</option>
											<option value="Dogo Canário">Dogo Canário</option>
											<option value="Dogue Alemão">Dogue Alemão</option>
											<option value="Dogue de Bordeaux">Dogue de Bordeaux</option>
											<option value="Elkhound Norueguês Cinza">Elkhound Norueguês Cinza</option>
											<option value="Fila Brasileiro">Fila Brasileiro</option>
											<option value="Flat­Coated Retriever">Flat­Coated Retriever</option>
											<option value="Fox Terrier Pelo Duro">Fox Terrier Pelo Duro</option>
											<option value="Fox Terrier Pelo Liso">Fox Terrier Pelo Liso</option>
											<option value="Foxhound Inglês">Foxhound Inglês</option>
											<option value="Galgo Espanhol">Galgo Espanhol</option>
											<option value="Golden Retriever">Golden Retriever</option>
											<option value="Grande Münsterländer">Grande Münsterländer</option>
											<option value="Greyhound">Greyhound</option>
											<option value="Griffon Belga">Griffon Belga</option>
											<option value="Griffon de Bruxelas">Griffon de Bruxelas</option>
											<option value="Husky Siberiano">Husky Siberiano</option>
											<option value="Ibizan Hound">Ibizan Hound</option>
											<option value="Irish Soft Coated Wheaten Terrier">Irish Soft Coated Wheaten Terrier</option>
											<option value="Irish Wolfhound">Irish Wolfhound</option>
											<option value="Jack Russell Terrier">Jack Russell Terrier</option>
											<option value="Kerry Blue Terrier">Kerry Blue Terrier</option>
											<option value="Komondor">Komondor</option>
											<option value="Kuvasz">Kuvasz</option>
											<option value="Labrador Retriever">Labrador Retriever</option>
											<option value="Lagotto Romagnolo">Lagotto Romagnolo</option>
											<option value="Lakeland Terrier">Lakeland Terrier</option>
											<option value="Leonberger">Leonberger</option>
											<option value="Lhasa Apso">Lhasa Apso</option>
											<option value="Malamute do Alasca">Malamute do Alasca</option>
											<option value="Maltês">Maltês</option>
											<option value="Mastiff">Mastiff</option>
											<option value="Mastim Espanhol">Mastim Espanhol</option>
											<option value="Mastino Napoletano">Mastino Napoletano</option>
											<option value="Mudi">Mudi</option>
											<option value="Nordic Spitz">Nordic Spitz</option>
											<option value="Norfolk Terrier">Norfolk Terrier</option>
											<option value="Norwich Terrier">Norwich Terrier</option>
											<option value="Old English Sheepdog">Old English Sheepdog</option>
											<option value="Papillon">Papillon</option>
											<option value="Parson Russell Terrier">Parson Russell Terrier</option>
											<option value="Pastor Alemão">Pastor Alemão</option>
											<option value="Pastor Beauceron">Pastor Beauceron</option>
											<option value="Pastor Belga">Pastor Belga</option>
											<option value="Pastor Bergamasco">Pastor Bergamasco</option>
											<option value="Pastor Branco">Pastor Branco Suíço</option>
											<option value="Pastor Briard">Pastor Briard</option>
											<option value="Pastor da Ásia Central">Pastor da Ásia Central</option>
											<option value="Pastor de Shetland">Pastor de Shetland</option>
											<option value="Pastor dos Pirineus">Pastor dos Pirineus</option>
											<option value="Pastor Maremano Abruzês">Pastor Maremano Abruzês</option>
											<option value="Pastor Polonês">Pastor Polonês</option>
											<option value="Pastor Polonês da Planície">Pastor Polonês da Planície</option>
											<option value="Pequeno Basset Griffon da Vendéia">Pequeno Basset Griffon da Vendéia</option>
											<option value="Pequeno Cão Leão">Pequeno Cão Leão</option>
											<option value="Pequeno Lebrel Italiano">Pequeno Lebrel Italiano</option>
											<option value="Pequeno Lebrel Italiano">Pequeno Lebrel Italiano</option>
											<option value="Perdigueiro Português">Perdigueiro Português</option>
											<option value="Petit Brabançon">Petit Brabançon</option>
											<option value="Pharaoh Hound">Pharaoh Hound</option>
											<option value="Pinscher Miniatura">Pinscher Miniatura</option>
											<option value="Podengo Canário">Podengo Canário</option>
											<option value="Podengo Português">Podengo Português</option>
											<option value="Pointer Inglês">Pointer Inglês</option>
											<option value="Poodle Anão">Poodle Anão</option>
											<option value="Poodle Médio">Poodle Médio</option>
											<option value="Poodle Standard">Poodle Standard</option>
											<option value="Poodle Toy">Poodle Toy</option>
											<option value="Pug">Pug</option>
											<option value="Puli">Puli</option>
											<option value="Pumi">Pumi</option>
											<option value="Rhodesian Ridgeback">Rhodesian Ridgeback</option>
											<option value="Rottweiler">Rottweiler</option>
											<option value="Saluki">Saluki</option>
											<option value="Samoieda">Samoieda</option>
											<option value="São Bernardo">São Bernardo</option>
											<option value="Schipperke">Schipperke</option>
											<option value="Schnauzer">Schnauzer</option>
											<option value="Schnauzer Gigante">Schnauzer Gigante</option>
											<option value="Schnauzer Miniatura">Schnauzer Miniatura</option>
											<option value="Scottish Terrier">Scottish Terrier</option>
											<option value="Sealyham Terrier">Sealyham Terrier</option>
											<option value="Setter Gordon">Setter Gordon</option>
											<option value="Setter Inglês">Setter Inglês</option>
											<option value="Setter Irlandês Vermelho">Setter Irlandês Vermelho</option>
											<option value="Setter Irlandês Vermelho e Branco">Setter Irlandês Vermelho e Branco</option>
											<option value="Shar­pei">Shar­pei</option>
											<option value="Shiba">Shiba</option>
											<option value="Shih­Tzu">Shih­Tzu</option>
											<option value="Silky Terrier Australiano">Silky Terrier Australiano</option>
											<option value="Skye Terrier">Skye Terrier</option>
											<option value="Smoushond Holandês">Smoushond Holandês</option>
											<option value="Spaniel Bretão">Spaniel Bretão</option>
											<option value="Spinone Italiano">Spinone Italiano</option>
											<option value="Spitz Alemão Anão">Spitz Alemão Anão</option>
											<option value="Spitz Finlandês">Spitz Finlandês</option>
											<option value="Spitz Japonês Anão">Spitz Japonês Anão</option>
											<option value="Springer Spaniel Inglês">Springer Spaniel Inglês</option>
											<option value="Stabyhoun">Stabyhoun</option>
											<option value="Staffordshire Bull Terrier">Staffordshire Bull Terrier</option>
											<option value="Terra Nova">Terra Nova</option>
											<option value="Terrier Alemão de caça Jagd">Terrier Alemão de caça Jagd</option>
											<option value="Terrier Brasileiro">Terrier Brasileiro</option>
											<option value="Terrier Irlandês de Glen do Imaal">Terrier Irlandês de Glen do Imaal</option>
											<option value="Terrier Preto da Rússia">Terrier Preto da Rússia</option>
											<option value="Tibetan Terrier">Tibetan Terrier</option>
											<option value="Tosa Inu">Tosa Inu</option>
											<option value="Vira­Latas">Vira­Latas</option>
											<option value="Vizsla">Vizsla</option>
											<option value="Volpino Italiano">Volpino Italiano</option>
											<option value="Weimaraner">Weimaraner</option>
											<option value="Welsh Corgi Cardigan">Welsh Corgi Cardigan</option>
											<option value="Welsh Corgi Pembroke">Welsh Corgi Pembroke</option>
											<option value="Welsh Springer Spaniel">Welsh Springer Spaniel</option>
											<option value="Welsh Terrier">Welsh Terrier</option>
											<option value="West Highland White Terrier">West Highland White Terrier</option>
											<option value="Whippet">Whippet</option>
											<option value="Yorkshire Terrier">Yorkshire Terrier</option>
										</optgroup>
  									<optgroup label="Gato">
											<option value="Abissínio">Abissínio</option>
											<option value="American Curl">American Curl</option>
											<option value="American Longhair">American Longhair</option>
											<option value="American Shorthair">American Shorthair</option>
											<option value="American Wirehair">American Wirehair</option>
											<option value="Angorá Turco">Angorá Turco</option>
											<option value="Balinês">Balinês</option>
											<option value="Bobtail Americano">Bobtail Americano</option>
											<option value="Bobtail Japonês">Bobtail Japonês</option>
											<option value="Bombaim">Bombaim</option>
											<option value="British Longhair">British Longhair</option>
											<option value="British Shorthair">British Shorthair</option>
											<option value="Burmês (Burmeses)">Burmês (Burmeses)</option>
											<option value="Burmilla">Burmilla</option>
											<option value="California Spangled">California Spangled</option>
											<option value="Chartreux">Chartreux</option>
											<option value="Colorpoint Shorthair">Colorpoint Shorthair</option>
											<option value="Cornish Rex">Cornish Rex</option>
											<option value="Devon Rex">Devon Rex</option>
											<option value="Exotic Shorthair">Exotic Shorthair</option>
											<option value="Gato Egeu">Gato Egeu</option>
											<option value="Gato Somali">Gato Somali</option>
											<option value="Gato­ de ­Bengala">Gato­ de­ Bengala</option>
											<option value="Havana Brown">Havana Brown</option>
											<option value="Himalaio">Himalaio</option>
											<option value="Javanese">Javanese</option>
											<option value="Korat">Korat</option>
											<option value="Kurilian Botail">Kurilian Botail</option>
											<option value="LaPerm">LaPerm</option>
											<option value="Levkoy Urcrâniano">Levkoy Urcrâniano</option>
											<option value="Maine Coon">Maine Coon</option>
											<option value="Manx">Manx</option>
											<option value="Mau egípcio">Mau egípcio</option>
											<option value="Mist Australiano">Mist Australiano</option>
											<option value="Munchkin">Munchkin</option>
											<option value="Nebelung">Nebelung</option>
											<option value="Norueguês da Floresta">Norueguês da Floresta</option>
											<option value="Ocicat">Ocicat</option>
											<option value="Oriental Bicolour">Oriental Bicolour</option>
											<option value="Oriental Shorthair">Oriental Shorthair</option>
											<option value="Persa">Persa</option>
											<option value="Peterbald">Peterbald</option>
											<option value="Pêlo Curto Brasileiro">Pêlo Curto Brasileiro</option>
											<option value="Pêlo Curto Europeu">Pêlo Curto Europeu</option>
											<option value="Pixie­Bob">Pixie­Bob</option>
											<option value="Ragamuffin">Ragamuffin</option>
											<option value="Ragdoll">Ragdoll</option>
											<option value="Russian Blue">Russian Blue</option>
											<option value="Sagrado da Birmânia">Sagrado da Birmânia</option>
											<option value="Savannah">Savannah</option>
											<option value="Scottish Fold">Scottish Fold</option>
											<option value="Selkirk Rex">Selkirk Rex</option>
											<option value="Serengeti">Serengeti</option>
											<option value="Siamês">Siamês</option>
											<option value="Snowshoe">Snowshoe</option>
											<option value="Sokoke">Sokoke</option>
											<option value="Sphynx">Sphynx</option>
											<option value="Tonquinês">Tonquinês</option>
											<option value="Toyger">Toyger</option>
											<option value="Turkish Van">Turkish Van</option>
											<option value="York Chocolate Cat">York Chocolate Cat</option>
										</optgroup>
									</select>
							</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-6">
							<div class="form-group">
									<label class="control-label">Porte</label>
									<select name="size" id="size" class="selectpicker" data-style="btn-default btn-block">
										<option value="Mini">Até 5 kg</option>
                    <option value="Pequeno">5­-15 kg</option>
                    <option value="Médio">15-25 kg</option>
                    <option value="Grande">25-40 kg</option>
                    <option value="Gigante">Mais de 40 kg</option>
									</select>
							</div>
					</div>
					<div class="col-md-6">
							<div class="form-group">
									<label class="control-label">Ração Favorita</label>
									<select name="ration" id="ration" class="selectpicker" data-style="btn-default btn-block">
										<option value="Cibau">Cibau</option>
                    <option value="Equilíbrio Total">Equilíbrio Total</option>
                    <option value="Vet Life">Vet Life</option>
                    <option value="Guabi Natural">Guabi Natural</option>
                    <option value="Royal Canin Breed">Royal Canin Breed</option>
                    <option value="VitalCan PRO">VitalCan PRO</option>
                    <option value="Frost Alisu">Frost Alisu</option>
                    <option value="Huggy">Huggy</option>
                    <option value="Hills – Science Diet">Hills – Science Diet</option>
                    <option value="Hills – Prescription Diet Canine">Hills – Prescription Diet Canine</option>
                    <option value="Eukanuba">Eukanuba</option>
                    <option value="Nestlé ProPlan Purina">Nestlé ProPlan Purina</option>
                    <option value="Premier Pet">Premier Pet</option>
                    <option value="Cyno">Cyno</option>
                    <option value="Pedigree Advanced">Pedigree Advanced</option>
                    <option value="Nutron Pet Wellness">Nutron Pet Wellness</option>
									</select>
							</div>
					</div>
					<div class="clearfix"></div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        <button type="button" class="btn btn-warning" onclick="userSave()">Salvar</button>
	      </div>
			{!! Form::close() !!}
    </div>
  </div>
</div>
<div class="modal fade schedule-cancel-modal" tabindex="-1" role="dialog" style="padding-top: 15rem">
  <div class="modal-dialog modal-sm" style="width: 400px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Cancelar Agendamento</h4>
      </div>
      <div class="modal-body" style="padding-top: 0;">
				<input type="hidden" id="petshop_id" name="petshop_id" value="{{ $petshopId }}">
				<input type="hidden" id="schedule_id" name="schedule_id">
				<div class="col-md-12">
						<div class="form-group">
								<p class="text-center">Descreva abaixo o motivo pelo qual o agendamento está sendo cancelado para que o cliente fique ciente do ocorrido de forma clara e detalhada.</p>
								<label class="control-label">Justificativa</label>
								<textarea class="form-control" id="justification" name="justification" placeholder=""></textarea>
						</div>
				</div>
				<div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-danger">Enviar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade service-details-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
				<p class="category"><span class="status-modal"></span><span class="status-card darker-gray"></span></p>
      </div>
      <div class="modal-body">
				<div class="col-md-12 center">
					<div class="images-container center">
						<div class="circle userimage pull-left"></div>
						<div class="circle petimage pull-left"></div>
					</div>
					<div class="info-container text-center">
						<div class="form-group">
							<label class="control-label">CLIENTE</label><br>
							<span class="username"></span>
						</div>
						<div class="form-group">
							<label class="control-label">PET</label><br>
							<span class="petname"></span> · <span class="breed"></span> · <span class="size"></span>
						</div>
						<div class="form-group">
							<label class="control-label">EMAIL</label><br>
							<span class="email"></span>
						</div>
						<div class="form-group">
							<label class="control-label">CONTATO</label><br>
							<span class="phone"></span>
						</div>
						<div class="form-group">
							<label class="control-label">OBSERVAÇÕES</label><br>
							<span class="note"></span>
						</div>
					</div>
				</div>
      </div>
      <div class="modal-footer">
				<div class="price pull-left">Total · <span class="second-color"></span></div>
        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Cancelar Agendamento</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade schedule-add-modal ajax-form" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
			{!! Form::open(array('url'=>'admin/calendar/add','method'=>'POST', 'id'=>'schedule-add-form')) !!}
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Adicionar Agendamento</h4>
	      </div>
	      <div class="modal-body">
					<input type="hidden" id="petshop_id" name="petshop_id" value="{{ $petshopId }}">
					<div class="col-md-4">
							<div class="form-group">
									<label class="control-label">Serviço</label>
									<select name="service_id" id="service_id" class="selectpicker" data-style="btn-default btn-block" data-live-search="true">
										@foreach($services as $service)
											<option value="{{ $service->id }}">{{ $service->name }}</option>
										@endforeach
									</select>
							</div>
					</div>
					<div class="col-md-4">
							<div class="form-group">
									<label class="control-label">Cliente</label>
									<select name="user_id" id="user_id" class="selectpicker" data-style="btn-default btn-block" data-live-search="true">
										@foreach($users as $user)
											<option value="{{ $user->id }}">{{ $user->name }}</option>
										@endforeach
									</select>
							</div>
					</div>
					<div class="col-md-4">
	            <label>Data/Hora</label>
	            <input type='text' class="form-control datetimepicker"/>
	        </div>
					<div class="clearfix"></div>
					<input type="hidden" id="pet_id" name="pet_id">
					<input type="hidden" id="date" name="date">
					<div class="col-md-12 center pet-grid"></div>
					<div class="clearfix"></div>
					<div class="col-md-12">
							<div class="form-group">
									<label class="control-label">Observações</label>
									<textarea name="note" id="note" class="form-control"></textarea>
							</div>
					</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        <button type="submit" class="btn btn-warning">Salvar</button>
	      </div>
			{!! Form::close() !!}
    </div>
  </div>
</div>
<div class="modal fade service-edit-modal ajax-form" tabindex="-1" role="dialog" style="padding-top: 15rem">
  <div class="modal-dialog modal-sm" style="width: 400px;">
    <div class="modal-content">
			{!! Form::open(array('url'=>'admin/services/update','method'=>'POST', 'id'=>'service-update-form')) !!}
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Edição do Serviço</h4>
	      </div>
	      <div class="modal-body">
					<input type="hidden" id="petshop_id" name="petshop_id" value="{{ $petshopId }}">
					<input type="hidden" id="service_id" name="service_id">
					<div class="col-md-6">
							<div class="form-group">
									<label class="control-label">Nome do Serviço</label>
									<input class="form-control" type="text" name="name" placeholder="Ex: Banho e Tosa" maxlength="16">
							</div>
					</div>
					<div class="col-md-6">
							<div class="form-group">
									<label class="control-label">Preço Original</label>
									<input class="form-control" type="text" name="price" placeholder="Ex: 100,00" maxlength="6">
							</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-6">
							<div class="form-group">
									<label>Promoção Ativa?</label>
									<div class="switch has-switch" data-on-label="SIM" data-off-label="NÃO">
										<div class="switch-on switch-animate">
											<input type="hidden" id="sale" class="sale" name="sale">
											<input type="checkbox" checked="" data-toggle="switch">
											<span class="switch-left"></span>
											<span class="switch-right"></span>
										</div>
									</div>
							</div>
					</div>
					<div class="col-md-6">
							<div class="form-group">
									<label class="control-label">Preço Promocional</label>
									<input class="form-control" type="text" name="new_price" placeholder="Ex: 80,00" maxlength="6">
							</div>
					</div>
	      </div>
	      <div class="modal-footer">
					<button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="serviceRemove()">Remover</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        <button type="submit" class="btn btn-warning">Salvar</button>
	      </div>
			{!! Form::close() !!}
    </div>
  </div>
</div>
<div class="modal fade service-create-modal" tabindex="-1" role="dialog" style="padding-top: 15rem">
  <div class="modal-dialog modal-sm" style="width: 400px;">
    <div class="modal-content">
			{!! Form::open(array('url'=>'admin/services/store','method'=>'POST', 'id'=>'service-create-form')) !!}
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Cadastrar Serviço</h4>
	      </div>
	      <div class="modal-body">
					<input type="hidden" id="petshop_id" name="petshop_id" value="{{ $petshopId }}">
					<div class="col-md-6">
							<div class="form-group">
									<label class="control-label">Nome do Serviço</label>
									<input class="form-control" type="text" name="name" placeholder="Ex: Banho e Tosa" maxlength="16">
							</div>
					</div>
					<div class="col-md-6">
							<div class="form-group">
									<label class="control-label">Preço Original</label>
									<input class="form-control" type="text" name="price" placeholder="Ex: 100,00" maxlength="6">
							</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-6">
							<div class="form-group">
									<label>Promoção Ativa?</label>
									<div class="switch has-switch" data-on-label="SIM" data-off-label="NÃO">
										<div class="switch-on switch-animate">
											<input type="hidden" id="sale" class="sale" name="sale">
											<input type="checkbox" checked="" data-toggle="switch">
											<span class="switch-left"></span>
											<span class="switch-right"></span>
										</div>
									</div>
							</div>
					</div>
					<div class="col-md-6">
							<div class="form-group">
									<label class="control-label">Preço Promocional</label>
									<input class="form-control" type="text" name="new_price" placeholder="Ex: 80,00" maxlength="6">
							</div>
					</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        <button type="button" class="btn btn-warning" onclick="serviceSave()">Salvar</button>
	      </div>
			{!! Form::close() !!}
    </div>
  </div>
</div>
<div class="modal fade update-duration-modal" tabindex="-1" role="dialog" style="padding-top: 15rem">
  <div class="modal-dialog modal-sm" style="width: 206px;">
    <div class="modal-content">
			{!! Form::open(array('url'=>'admin/schedules/updateDuration','method'=>'POST', 'id'=>'update-duration-form')) !!}
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h3 class="modal-title text-center">Duração</h3>
	      </div>
	      <div class="modal-body">
					<input type="hidden" id="petshop_id" name="petshop_id" value="{{ $petshopId }}">
					<div class="col-md-12 text-center">
							<!-- <div class="form-group">
									<input class="form-control" type="number" name="duration" placeholder="Ex: 1">
							</div> -->
							<select class="drum">
								<option selected value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
							<h4 style="margin-top: 0">horas</h4>
					</div>
					<div class="clearfix"></div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        <button type="button" class="btn btn-warning" onclick="updateDuration({{ $petshopId }})">Salvar</button>
	      </div>
			{!! Form::close() !!}
    </div>
  </div>
</div>
<div class="modal fade push-create-modal ajax-form" tabindex="-1" role="dialog" style="padding-top: 15rem">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
			{!! Form::open(array('url'=>'admin/notifications/store','method'=>'POST', 'id'=>'push-create-form')) !!}
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Enviar Push</h4>
	      </div>
	      <div class="modal-body">
					<input type="hidden" id="petshop_id" name="petshop_id" value="{{ $petshopId }}">
					<div class="col-md-12">
							<div class="form-group">
									<label class="control-label">Cliente</label>
									<select name="user_id" id="user_id" class="selectpicker" data-style="btn-default btn-block" data-live-search="true">
										@foreach($users as $user)
											<option value="{{ $user->id }}">{{ $user->name }}</option>
										@endforeach
									</select>
							</div>
					</div>
					<div class="col-md-12">
							<div class="form-group">
									<label class="control-label">Título</label>
									<input class="form-control" type="text" id="title" name="title" placeholder="Ex: Serviço Finalizado">
							</div>
					</div>
					<div class="col-md-12">
							<div class="form-group">
									<label class="control-label">Mensagem</label>
									<input class="form-control" type="text" id="message" name="message" placeholder="Ex: Seu Pet está esperando por você">
							</div>
					</div>
					<div class="clearfix"></div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        <button type="submit" class="btn btn-warning">Salvar</button>
	      </div>
			{!! Form::close() !!}
    </div>
  </div>
</div>
</body>

  <!--   Core JS Files   -->
  <script src="{{ asset('js/jquery-1.10.2.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/jquery.bootstrap.wizard.js') }}"></script>
	<script src="{{ asset('js/bootstrap-clockpicker.min.js') }}"></script>
	<script src="{{ asset('js/sweetalert.min.js') }}"></script>
	<script src="{{ asset('js/hammer.min.js') }}"></script>
	<script src="{{ asset('js/hammer.fakemultitouch.js') }}"></script>
	<script src="{{ asset('js/drum.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-notify.min.js') }}"></script>
	<!-- <script src="{{ asset('js/jquery.validate.min.js') }}"></script> -->

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="{{ asset('js/bootstrap-checkbox-radio-switch.js') }}"></script>

	<!--  Charts Plugin -->
	<script src="{{ asset('js/chartist.min.js') }}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{ asset('js/bootstrap-notify.js') }}"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="{{ asset('js/light-bootstrap-dashboard.js') }}"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="{{ asset('js/demo.js') }}"></script>

	<script src="{{ asset('js/datatables.js') }}"></script>
	<script src="{{ asset('js/bootstrap-table.js') }}"></script>
	<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
	<script src="{{ asset('js/moment-with-locales.js') }}"></script>
	<script src="{{ asset('js/fullcalendar.min.js') }}"></script>
	<script src="{{ asset('js/pt-br.js') }}"></script>
	<script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
	<script src="{{ asset('js/bootstrap-checkbox-radio-switch.js') }}"></script>
	<!--	Custom JS  -->
	<script src="{{ asset('js/app.js') }}"></script>

	<script src="{{ asset('js/wizard.js') }}"></script>
</html>
