@extends('owner')

@section('content')
<style>
.navbar-brand{
  text-align: center;
}
</style>
<div class="container-fluid no-padding-sides">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-wizard wizard-card ct-wizard-orange" id="wizardCard">
        <div class="header">
            <h4 class="title">Novos Pet Shops</h4>
            <p class="category">Pet shops aguardando aprovação</p>
        </div>
        <div class="content">
          <div class="row petshop-grid schedules-grid">
            @forelse($waiting as $waiting)
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <div class="col-md-12" style="padding: 0">
                  <a>
                    <div class="petshop-container schedule-container waiting bg-gray" style="background-image: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url({{ $waiting->back_image }})">
                      <div class="user-image back-image" style="background-image: url({{ $waiting->image }})" onclick="petshopDetails({{ $waiting->id }})"></div>
                      <h3 onclick="petshopDetails({{ $waiting->id }})">{{ $waiting->name }}</h3>
                      <h4><span>Cidade</span> · {{ $waiting->city }} <br><span>Tel</span> · {{ $waiting->phone }}</h4>
                      <div class="action-status-btn aprove" onclick="updatePetshopStatus({{ $waiting->id }})">
                        <i class="pe-7s-unlock white"></i>
                        <h6>Aprovar</h6>
                      </div>
                    </div>
                    <h4 class="time waiting-time bg-main-color">{{ $waiting->address }}, {{ $waiting->number }} - {{ $waiting->district }}</h4>
                  </a>
                </div>
              </div>
            @empty
              <div class="alert alert-danger text-center">
                  <span><b>Não existem pet shops aguardando aprovação</b></span>
              </div>
            @endforelse
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-wizard wizard-card ct-wizard-orange" id="wizardCard">
        <div class="header">
            <div class="pull-left">
              <h4 class="title">Gestão de Pet Shops</h4>
              <p class="category">Gerencie os pet shops cadastrados no LinkedPet</p>
            </div>
            <div class="pull-right">
              <!-- <button class="btn btn-simple btn-wd btn-warning" onclick="petshopDetails()">+ Adicionar Petshop</button> -->
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <div class="content" style="padding: 0;">
          <div class="row table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th class="text-center">
                    <div class="th-inner"></div>
                    <div class="fht-cell"></div>
                  </th>
                  <th data-field="name">
                    <div class="th-inner sortable both">Nome</div>
                    <div class="fht-cell"></div>
                  </th>
                  <th data-field="salary">
                    <div class="th-inner sortable both">Telefone</div>
                    <div class="fht-cell"></div>
                  </th>
                  <th data-field="salary">
                    <div class="th-inner sortable both">CNPJ</div>
                    <div class="fht-cell"></div>
                  </th>
                </tr>
              </thead>
              <tbody>
                @forelse($petshops as $petshop)
                  <tr data-index="0" onclick="petshopDetails({{ $petshop->id }})">
                    <td class="back-image" style="padding: 5px 25px !important; background-image: url({{ $petshop->back_image ?: '/images/default.png' }}), linear-gradient(#000, #000);"><div class="user-image circle center" style="background-image: url({{ $petshop->image ?: '/images/default.png' }})"></div></td>
                    <td>{{ $petshop->name }}</td>
                    <td>{{ $petshop->phone }}</td>
                    <td>{{ $petshop->cnpj }}</td>
                  </tr>
                @empty
                  <div class="alert alert-danger text-center">
                      <span><b>Não existem usuários cadastrados</b></span>
                  </div>
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-wizard wizard-card ct-wizard-orange" id="wizardCard">
        <div class="header">
            <div class="pull-left">
              <h4 class="title">Gestão de Usuários</h4>
              <p class="category">Gerencie os usuários cadastrados no LinkedPet</p>
            </div>
            <div class="pull-right">
              <button class="btn btn-simple btn-wd btn-warning" onclick="userCreate()">+ Adicionar Usuário</button>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <div class="content" style="padding: 0;">
          <div class="row table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th class="text-center">
                    <div class="th-inner"></div>
                    <div class="fht-cell"></div>
                  </th>
                  <th data-field="name">
                    <div class="th-inner sortable both">Nome</div>
                    <div class="fht-cell"></div>
                  </th>
                  <th data-field="salary">
                    <div class="th-inner sortable both">Email</div>
                    <div class="fht-cell"></div>
                  </th>
                </tr>
              </thead>
              <tbody>
                @forelse($users as $user)
                  <tr data-index="0">
                    <td style="padding: 5px 25px !important;"><div class="user-image circle center" style="background-image: url({{ $user->image ?: '/images/default.png' }})"></div></td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                  </tr>
                @empty
                  <div class="alert alert-danger text-center">
                      <span><b>Não existem usuários cadastrados</b></span>
                  </div>
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
