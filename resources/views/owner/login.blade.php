@extends('initial')

@section('content')
<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
        <a class="navbar-brand logo logo-transparent" href="http://www.linkedpet.com.br"></a>
    </div>
</nav>
<div class="wrapper wrapper-full-page">
    <div class="full-page owner-login-page login-page" data-color="black">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <form role="form" method="POST" action="{{ url('/owner/login') }}">
                            <div class="card">
                                <div class="content">
                                    @if (count($errors) > 0)
                                      <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                    @endif
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="password" id="password" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <span class="icons"><span class="first-icon fa fa-square-o"></span><span class="second-icon fa fa-check-square-o"></span></span><input type="checkbox" data-toggle="checkbox" value="">
                                            Continuar Conectado
                                        </label>
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <button type="submit" class="btn btn-fill btn-default btn-wd">Entrar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    	<footer class="footer footer-transparent">
            <div class="container">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="#">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                A Empresa
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    © 2016 <a href="http://www.linkedpet.com.br">LinkedPet</a>
                </p>
            </div>
        </footer>

    <div class="full-page-background" style="background-image: url(../../assets/img/full-screen-image-1.jpg) "></div></div>

</div>
@endsection
