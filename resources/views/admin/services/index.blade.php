@extends('template')

@section('content')
<style>
.navbar-brand{
  text-align: center;
}
</style>
<div class="container-fluid no-padding-sides">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-wizard wizard-card ct-wizard-orange" id="wizardCard">
          <div class="header">
              <div class="pull-left">
                <h4 class="title">Gestão de Serviços</h4>
                <p class="category">Gerencie os serviços disponíveis em seu petshop</p>
              </div>
              <div class="pull-right">
                <button class="btn btn-simple btn-wd btn-warning" onclick="serviceCreate()">+ Adicionar Serviço</button>
              </div>
              <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
          <div class="content">
            <div class="row petshop-grid service-grid">
              @foreach($services as $service)
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2" onclick="serviceEditModal({{ $service->id }}, '{{ $service->name }}', '{{ $service->price }}', '{{ $service->new_price }}', '{{ $service->sale }}')">
                  <div class="col-md-12" style="padding: 0">
                    <a>
                      <div class="petshop-container schedule-container text-center" style="background-image: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url('')">
                        <div class="user-image back-image bg-second-color white pe-7s-tools center"></div>
                        <div class="clearfix"></div>
                        <h6 class="text-center grayer" onclick="serviceEdit({{ $service->id }})">{{ $service->name }}</h6>
                        <p class="text-center gray price"><span>Preço</span> · R${{ $service->price }}</p>
                        <div class="clearfix"></div>
                      </div>
                    </a>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
