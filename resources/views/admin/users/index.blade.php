@extends('template')

@section('content')
<style>
.navbar-brand{
  text-align: center;
}
</style>
<div class="container-fluid no-padding-sides">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-wizard wizard-card ct-wizard-orange" id="wizardCard">
          <div class="header">
              <div class="pull-left">
                <h4 class="title">Gestão de Usuários</h4>
                <p class="category">Gerencie os usuários cadastrados no LinkedPet</p>
              </div>
              <div class="pull-right">
                <button class="btn btn-simple btn-wd btn-warning" onclick="userCreate()">+ Adicionar Usuário</button>
              </div>
              <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
          <div class="content" style="padding: 0;">
            <div class="row">
              <table id="bootstrap-table" class="table">
                <thead>
                  <tr>
                    <th class="text-center">
                      <div class="th-inner"></div>
                      <div class="fht-cell"></div>
                    </th>
                    <th data-field="name">
                      <div class="th-inner sortable both">Nome</div>
                      <div class="fht-cell"></div>
                    </th>
                    <th data-field="salary">
                      <div class="th-inner sortable both">Email</div>
                      <div class="fht-cell"></div>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($users as $user)
                    <tr data-index="0">
                      <td style="padding: 5px 25px !important;"><div class="user-image circle center" style="background-image: url({{ $user->image ?: '/images/default.png' }})"></div></td>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                    </tr>
                  @empty
                    <div class="alert alert-danger text-center">
                        <span><b>Não existem usuários cadastrados</b></span>
                    </div>
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
