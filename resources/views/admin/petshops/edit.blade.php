@extends('template')

@section('content')
<style>
.navbar-brand{
  text-align: center;
}
</style>
<div class="container-fluid no-padding-sides ajax-form">
  {!! Form::open(array('url'=>'admin/petshops/update', 'method'=>'POST', 'files'=>true)) !!}
    <div class="row">
      <input type="hidden" name="petshop_id" id="petshop_id" value="{{ $petshop->id }}">
      <div class="col-md-8">
          <div class="card">
              <div class="header">
                  <h4 class="title">Editar Petshop</h4>
              </div>
              <div class="content">
                <div class="row"> 
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>CNPJ</label>
                            <input type="text" class="form-control" name="cnpj" placeholder="Ex: 13543289520948" value="{{ $petshop->cnpj }}">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="name" placeholder="Ex: Nome do Petshop" value="{{ $petshop->name }}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Telefone</label>
                            <input type="phone" class="form-control" name="phone" placeholder="Ex: 11912345678" value="{{ $petshop->phone }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Endereço</label>
                            <input type="text" class="form-control" name="address" placeholder="Ex: Av. Paulista" value="{{ $petshop->address }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Número</label>
                            <input type="number" class="form-control" name="number" placeholder="Ex: 2202" value="{{ $petshop->number }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Bairro</label>
                            <input type="text" class="form-control" name="district" placeholder="Ex: Consolação" value="{{ $petshop->district }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Cidade</label>
                            <input type="text" class="form-control" name="city" placeholder="Ex: Cidade" value="{{ $petshop->city }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <select name="state" class="form-control" name="state" value="{{ $petshop->state }}">
                                <option selected="" disabled="">Selecione o Estado...</option>
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AP">Amapá</option>
                                <option value="AM">Amazonas</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>
                                <option value="ES">Espirito Santo</option>
                                <option value="GO">Goiás</option>
                                <option value="MA">Maranhão</option>
                                <option value="MS">Mato Grosso do Sul</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>
                                <option value="RJ">Rio de Janeiro</option>
                                <option value="RN">Rio Grande do Norte</option>
                                <option value="RS">Rio Grande do Sul</option>
                                <option value="RO">Rondônia</option>
                                <option value="RR">Roraima</option>
                                <option value="SC">Santa Catarina</option>
                                <option value="SP">São Paulo</option>
                                <option value="SE">Sergipe</option>
                                <option value="TO">Tocantins</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Complemento</label>
                            <input type="text" class="form-control" name="complement" placeholder="Ex: Conjunto 123" value="{{ $petshop->complement }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Descrição</label>
                            <textarea rows="5" class="form-control" name="description" placeholder="Ex: Petshop especializado em cães grandes">{{ $petshop->description }}</textarea>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-fill bg-second-color pull-right no-border">Atualizar Informações</button>
                <div class="clearfix"></div>
              </div>
          </div>
      </div>
      <div class="col-md-4">
          <div class="card card-user">
              {!! Form::file('back_image', ['id' => 'back_image']) !!}
              {!! Form::file('image', ['id' => 'image']) !!}
              <div class="image">
                  <label for="back_image"><img class="petshop-back-image" src="{{ $petshop->back_image }}" alt="..."></label>
              </div>
              <div class="content">
                  <div class="author">
                       <a href="#">
                      <label for="image"><img class="avatar border-gray petshop-image" src="{{ $petshop->image }}" alt="..."></label>
                        <h4 class="title">{{ $petshop->name }}<br>
                           <!-- Petshop Rating -->
                        </h4>
                      </a>
                  </div>
                  <p class="description text-center">{{ $petshop->description }}</p>
              </div>
              <hr>
              <div class="text-center">
                  <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                  <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                  <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>
              </div>
          </div>
      </div>
    </div>
  {!! Form::close() !!}
</div>
@endsection
