@extends('template')

@section('content')
<style>
.navbar-brand{
  text-align: center;
}
</style>
<div class="container-fluid no-padding-sides ajax-form">
  {!! Form::open(array('url'=>'admin/petshops/operationUpdate', 'method'=>'POST')) !!}
    <div class="row">
      <input type="hidden" name="petshop_id" id="petshop_id" value="{{ $petshop->id }}">
      <div class="col-md-8">
          <div class="card">
              <div class="header">
                  <h4 class="title">Horários de Funcionamento</h4>
              </div>
              <div class="content">
                <div class="form-group">
                    <div class="row">
                      <input type="hidden" name="weekdays_hours" id="weekdays_hours" value="{{ $petshop->weekdays_hours }}">
                      <label class="control-label pull-left">Horário (dias úteis)</label>
                      <div class="clearfix"></div>
                      <div class="col-md-3 clockpicker">
                          <div class="form-group">
                              <input type="text" class="form-control" id="ini-uteis" value="9:00">
                          </div>
                      </div>
                      <div class="col-md-1" style="padding-top: 1.3rem; text-align: center;">
                        <span>até</span>
                      </div>
                      <div class="col-md-3 clockpicker">
                          <div class="form-group">
                              <input type="text" class="form-control" id="fim-uteis" value="18:00">
                          </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="select-boxes-container">
                        <div class="weekdays-box row" style="margin-bottom: 3rem;">
                          <input type="hidden" name="weekdays" id="weekdays" value="{{ $petshop->weekdays }}">
                          <div class="col-md-2">
                            <div class="box bg-gray darker-gray SEG">
                              <span>SEG</span><br>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="box bg-gray darker-gray TER">
                              <span>TER</span><br>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="box bg-gray darker-gray QUA">
                              <span>QUA</span><br>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="box bg-gray darker-gray QUI">
                              <span>QUI</span><br>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="box bg-gray darker-gray SEX">
                              <span>SEX</span><br>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <input type="hidden" name="weekends_hours" id="weekends_hours" value="{{ $petshop->weekends_hours }}">
                          <label class="control-label pull-left">Horário (fins de semana)</label>
                          <div class="clearfix"></div>
                          <div class="col-md-3 clockpicker">
                              <div class="form-group">
                                  <input type="text" class="form-control" id="ini-fds" value="09:00">
                              </div>
                          </div>
                          <div class="col-md-1" style="padding-top: 1.3rem; text-align: center;">
                            <span>até</span>
                          </div>
                          <div class="col-md-3 clockpicker">
                              <div class="form-group">
                                  <input type="text" class="form-control" id="fim-fds" value="18:00">
                              </div>
                          </div>
                        </div>
                        <div class="weekends-box row">
                          <input type="hidden" name="weekends" id="weekends" value="{{ $petshop->weekends }}">
                          <div class="col-md-2">
                            <div class="box bg-gray darker-gray SAB">
                              <span>SAB</span><br>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="box bg-gray darker-gray DOM">
                              <span>DOM</span><br>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-fill bg-second-color pull-right no-border">Salvar</button>
                <div class="clearfix"></div>
              </div>
          </div>
      </div>
      <div class="col-md-4">
          <div class="card card-user">
              <div class="image">
                  <img class="petshop-back-image" src="{{ $petshop->back_image }}" alt="...">
              </div>
              <div class="content">
                  <div class="author">
                       <a href="#">
                       <img class="avatar border-gray petshop-image" src="{{ $petshop->image }}" alt="...">
                       <h4 class="title">{{ $petshop->name }}<br>
                           <!-- Petshop Rating -->
                        </h4>
                      </a>
                  </div>
                  <p class="description text-center">{{ $petshop->description }}</p>
              </div>
              <hr>
              <div class="text-center">
                  <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                  <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                  <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>
              </div>
          </div>
      </div>
    </div>
  {!! Form::close() !!}
</div>
@endsection
