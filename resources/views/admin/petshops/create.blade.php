@extends('template')

@section('content')
<style>
.sidebar, .nav .fa, .collapse.navbar-collapse{
  display: none !important;
}
.navbar{
  display: none;
}
.navbar-header{
  float: none;
}
.navbar-brand{
  text-align: center;
  width: 100%;
}
.main-panel{
  width: 100%;
}
</style>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-7 col-lg-5 petshop-insert">
        <div class="card card-wizard wizard-card ct-wizard-orange ajax-form" id="wizardCard">
              <div class="header text-center">
                  <h3 class="title">Cadastre seu Petshop</h3>
                  <p class="category">Já é um ótimo começo!</p>
              </div>
              {!! Form::open(array('url'=>'admin/petshops/store','method'=>'POST', 'files'=>true)) !!}
                <div class="content">
                    <ul class="nav nav-pills petshop-create">
                      <li class="active bg-darker-gray" style="width: 33.3333%;"><a href="#tab1" class="white" data-toggle="tab" aria-expanded="true">Petshop</a></li>
                      <li class="bg-darker-gray" style="width: 33.3333%;"><a href="#tab2" class="white" data-toggle="tab">Localização</a></li>
                      <li class="bg-darker-gray" style="width: 33.3333%;"><a href="#tab3" class="white" data-toggle="tab">Funcionamento</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                          <!-- <h4 class="text-center">Informações Básicas</h4> -->
                          <div class="row">
                            <div class="col-md-12 petshop-back-image">
                              <div class="col-md-4"><label for="back_image">+ Adicionar Imagem do Petshop</label></div>
                              <div class="col-md-4">
                                <label for="image">
                                  <div class="petshop-image bg-gray text-center">
                                    <span>+</span><br>Adicionar<br>Logo
                                  </div>
                                </label>
                                {!! Form::file('back_image', ['id' => 'back_image']) !!}
                                {!! Form::file('image', ['id' => 'image']) !!}
                              </div>
                              <div class="col-md-4"></div>
                            </div>
                            <div class="col-md-10 col-md-offset-1" style="padding-top: 2rem">
                                <div class="form-group">
                                    <label class="control-label">Nome do Pet shop</label>
                                    <input class="form-control" type="text" name="name">
                                </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-5 col-md-offset-1" style="margin-left: 7%">
                                <div class="form-group">
                                    <label class="control-label">CNPJ (apenas números)</label>
                                    <input class="form-control" type="text" name="cnpj" placeholder="ex: 01234567890123" maxlength="14">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">Telefone (apenas números)</label>
                                    <input class="form-control" type="text" name="phone" placeholder="ex: 11912345678" maxlength="11">
                                </div>
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="form-group">
                                    <label>Descrição</label>
                                    <textarea class="form-control" name="description"></textarea>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                          <h4 class="text-center">Onde todos vão te encontrar?</h4>
                          <div class="row">
                              <div class="col-md-10 col-md-offset-1">
                                  <div class="form-group">
                                      <label class="control-label">Endereço</label>
                                      <input class="form-control" type="text" name="address" placeholder="ex: Av. Paulista">
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-2 col-md-offset-1">
                                  <div class="form-group">
                                      <label class="control-label">Número</label>
                                      <input class="form-control" type="text" name="number" placeholder="ex: 1234">
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class="control-label">Complemento</label>
                                      <input class="form-control" type="text" name="complement" placeholder="ex: Conjunto 123">
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label class="control-label">Bairro</label>
                                      <input class="form-control" type="text" name="district" placeholder="ex: Bela Vista">
                                  </div>
                              </div>
                          </div>
                          <div class="row">

                          </div>
                          <div class="row">
                              <div class="col-md-5 col-md-offset-1">
                                  <div class="form-group">
                                      <label class="control-label">Cidade</label>
                                      <input class="form-control" type="text" name="city" placeholder="ex: São Paulo">
                                  </div>
                              </div>
                              <div class="col-md-5">
                                  <div class="form-group">
                                      <label class="control-label">Estado</label>
                                      <select name="state" class="form-control">
                                          <option selected="" disabled="">Selecione o Estado...</option>
                                          <option value="AC">Acre</option>
                                        	<option value="AL">Alagoas</option>
                                        	<option value="AP">Amapá</option>
                                        	<option value="AM">Amazonas</option>
                                        	<option value="BA">Bahia</option>
                                        	<option value="CE">Ceará</option>
                                        	<option value="DF">Distrito Federal</option>
                                        	<option value="ES">Espirito Santo</option>
                                        	<option value="GO">Goiás</option>
                                        	<option value="MA">Maranhão</option>
                                        	<option value="MS">Mato Grosso do Sul</option>
                                        	<option value="MT">Mato Grosso</option>
                                        	<option value="MG">Minas Gerais</option>
                                        	<option value="PA">Pará</option>
                                        	<option value="PB">Paraíba</option>
                                        	<option value="PR">Paraná</option>
                                        	<option value="PE">Pernambuco</option>
                                        	<option value="PI">Piauí</option>
                                        	<option value="RJ">Rio de Janeiro</option>
                                        	<option value="RN">Rio Grande do Norte</option>
                                        	<option value="RS">Rio Grande do Sul</option>
                                        	<option value="RO">Rondônia</option>
                                        	<option value="RR">Roraima</option>
                                        	<option value="SC">Santa Catarina</option>
                                        	<option value="SP">São Paulo</option>
                                        	<option value="SE">Sergipe</option>
                                        	<option value="TO">Tocantins</option>
                                      </select>
                                  </div>
                              </div>
                          </div>
                        </div>
                      <div class="tab-pane" id="tab3">
                        <!-- <h2 class="text-center text-space">Yuhuuu! <br><small> Click on "<b>Finish</b>" to join our community</small></h2> -->
                        <h4 class="text-center">Agora só nos falta saber quando...</h4>
                        <div class="col-md-11 select-boxes">
                            <div class="form-group">
                                <div class="row">
                                  <input type="hidden" name="weekdays_hours" id="weekdays_hours">
                                  <label class="control-label pull-left">Horário (dias úteis)</label>
                                  <div class="clearfix"></div>
                                  <div class="col-md-3 clockpicker">
                                      <div class="form-group">
                                          <input type="text" class="form-control" id="ini-uteis" value="09:00">
                                      </div>
                                  </div>
                                  <div class="col-md-1" style="padding-top: 1.3rem; text-align: center;">
                                    <span>até</span>
                                  </div>
                                  <div class="col-md-3 clockpicker">
                                      <div class="form-group">
                                          <input type="text" class="form-control" id="fim-uteis" value="18:00">
                                      </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="select-boxes-container">
                                    <div class="weekdays-box row" style="margin-bottom: 3rem;">
                                      <input type="hidden" name="weekdays" id="weekdays">
                                      <div class="col-md-2">
                                        <div class="box bg-gray darker-gray">
                                          <span>SEG</span><br>
                                        </div>
                                      </div>
                                      <div class="col-md-2">
                                        <div class="box bg-gray darker-gray">
                                          <span>TER</span><br>
                                        </div>
                                      </div>
                                      <div class="col-md-2">
                                        <div class="box bg-gray darker-gray">
                                          <span>QUA</span><br>
                                        </div>
                                      </div>
                                      <div class="col-md-2">
                                        <div class="box bg-gray darker-gray">
                                          <span>QUI</span><br>
                                        </div>
                                      </div>
                                      <div class="col-md-2">
                                        <div class="box bg-gray darker-gray">
                                          <span>SEX</span><br>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <input type="hidden" name="weekends_hours" id="weekends_hours">
                                      <label class="control-label pull-left">Horário (fins de semana)</label>
                                      <div class="clearfix"></div>
                                      <div class="col-md-3 clockpicker">
                                          <div class="form-group">
                                              <input type="text" class="form-control" id="ini-fds" value="09:00">
                                          </div>
                                      </div>
                                      <div class="col-md-1" style="padding-top: 1.3rem; text-align: center;">
                                        <span>até</span>
                                      </div>
                                      <div class="col-md-3 clockpicker">
                                          <div class="form-group">
                                              <input type="text" class="form-control" id="fim-fds" value="18:00">
                                          </div>
                                      </div>
                                    </div>
                                    <div class="weekends-box row">
                                      <input type="hidden" name="weekends" id="weekends">
                                      <div class="col-md-2">
                                        <div class="box bg-gray darker-gray">
                                          <span>SAB</span><br>
                                        </div>
                                      </div>
                                      <div class="col-md-2">
                                        <div class="box bg-gray darker-gray">
                                          <span>DOM</span><br>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wizard-footer">
                      <div class="pull-right">
                          <input type='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm btnNext' name='next' value='Próximo'/>
                          <input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Salvar'/>
                      </div>
                      <div class="pull-left">
                          <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm btnPrevious' name='previous' value='Voltar' />
                      </div>
                      <div class="clearfix"></div>
                  </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
