@extends('template')

@section('content')
<style>
.sidebar, .nav .fa, .collapse.navbar-collapse{
  display: none !important;
}
.navbar{
  display: none;
}
.navbar-header{
  float: none;
}
.navbar-brand{
  text-align: center;
  width: 100%;
}
.main-panel{
  width: 100%;
}
.footer{
  display: none;
}
</style>
<div class="container-fluid">
    <div class="row">
      <div class="col-md-8 col-lg-5 petshop-insert">
        <div class="card card-wizard wizard-card ct-wizard-orange" id="wizardCard">
          <div class="header text-center">
              <h3 class="title">Escolha seu Petshop</h3>
              <!-- <p class="category"></p> -->
          </div>
          <div class="content">
            <div class="row petshop-grid petshop-select">
              @foreach($petshops as $petshop)
                <div class="col-md-6">
                  <div class="col-md-12" style="margin: 2.5rem 0; padding: 0">
                    <a href="/admin/dashboard/index/{{$petshop->id}}">
                      <div class="petshop-container bg-gray" style="background-image: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url({{ $petshop->back_image }})">
                        <h4>{{ $petshop->name }}</h4>
                        <h5>{{ $petshop->district }}, {{ $petshop->city }}/{{ $petshop->state }}</h5>
                        <div class="petshop-logo" style="background-image: url({{ $petshop->image }})"></div>
                      </div>
                    </a>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
