@extends('template')

@section('content')
<style>
.navbar-brand{
  text-align: center;
}
</style>
<div class="container-fluid no-padding-sides">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-wizard wizard-card ct-wizard-orange" id="wizardCard">
          <div class="header">
              <div class="pull-left">
                <h4 class="title">Envio de Push Notications</h4>
                <p class="category">Atualize os usuários do LinkedPet com as novidades do seu petshop</p>
              </div>
              <div class="pull-right">
                <button class="btn btn-simple btn-wd btn-warning" onclick="pushCreateModal()">+ Criar Push</button>
              </div>
              <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
          <div class="content" style="padding: 0;">
            <div class="row">
              <table id="bootstrap-table" class="table">
                <thead>
                  <tr>
                    <th class="text-center">
                      <div class="th-inner"></div>
                      <div class="fht-cell"></div>
                    </th>
                    <th data-field="name">
                      <div class="th-inner sortable both">Nome</div>
                      <div class="fht-cell"></div>
                    </th>
                    <th data-field="salary">
                      <div class="th-inner sortable both">Título</div>
                      <div class="fht-cell"></div>
                    </th>
                    <th data-field="salary">
                      <div class="th-inner sortable both">Mensagem</div>
                      <div class="fht-cell"></div>
                    </th>
                    <th data-field="salary">
                      <div class="th-inner sortable both">Data</div>
                      <div class="fht-cell"></div>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($notifications as $notification)
                    <tr>
                      <td style="padding: 5px 25px !important;"><div class="user-image circle center" style="background-image: url({{ $notification->image ?: '/images/default.png' }})"></div></td>
                      <td>{{ $notification->name }}</td>
                      <td>{{ $notification->title }}</td>
                      <td>{{ $notification->message }}</td>
                      <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $notification->created_at)->subHours(3)->formatLocalized('%A, %d de %B de %Y às %Hh%M') }}</td>
                    </tr>
                  @empty
                    <div class="alert alert-danger text-center">
                        <span><b>Não existem usuários cadastrados</b></span>
                    </div>
                  @endforelse
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
