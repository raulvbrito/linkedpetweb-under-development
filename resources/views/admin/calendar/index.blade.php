@extends('template')

@section('content')
<style>
.navbar-brand{
  text-align: center;
}
</style>
<div class="container-fluid no-padding-sides">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-calendar ct-wizard-orange">
          <div class="content">
            <div id="fullCalendar"></div>
          </div>
        </div>
      </div>
      <div class="col-md-12 hide-on-print">
        <a href="javascript:window.print()"><i class="pe-7s-print main-color center"></i></a>
      </div>
    </div>
</div>
@endsection
