@extends('template')

@section('content')
<style>
.navbar-brand{
  text-align: center;
}
</style>
<div class="container-fluid no-padding-sides">
    <div class="row">
      <!--  Menu: Conterá foto do usuário, opções de perfil dashboard (home), agenda e gerenciamento dos serviços/horários -->
      <!--  Primeira linha: Serviços em andamento, com foto do pet, tìtulo do serviço, status, duração total/restante-->
      <!--  Segunda linha: Próximos serviços agendados, com foto do pet, tìtulo do serviço, duração total/restante-->
      <!--  Botões de ação para atualização de status: Aprovar, Iniciar, Finalizar-->
      <div class="col-md-12">
        <div class="card card-wizard wizard-card ct-wizard-orange" id="wizardCard">
          <div class="header">
              <h4 class="title">Serviços em andamento</h4>
              <p class="category">Mantenha seus clientes atualizados</p>
          </div>
          <div class="content">
            <div class="row petshop-grid schedules-grid">
              @forelse($ongoings as $ongoing)
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                  <div class="col-md-12" style="padding: 0">
                    <a>
                      <div class="petshop-container schedule-container bg-gray" style="background-image: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url({{ $ongoing->pet_image }})">
                        <div class="user-image back-image" style="background-image: url({{ $ongoing->pet_image }})" onclick="serviceDetails({{ $ongoing->schedule_id }})"></div>
                        <h3 onclick="serviceDetails({{ $ongoing->schedule_id }})">{{ $ongoing->service_name }}</h3>
                        <h4><span>Cliente</span> · {{ $ongoing->username }} <br><span>Pet</span> · {{ $ongoing->pet_name }}</h4>
                        <div class="action-status-btn finish" onclick="updateServiceStatus('finish', {{ $ongoing->schedule_id }}, {{ $petshopId }})">
                          <i class="fa pe-7s-check white"></i>
                          <h6>Finalizar</h6>
                        </div>
                      </div>
                      <h4 class="time time-to-finish bg-second-color"><i class="fa fa-clock-o white"></i>Término · <span><span class="duration">{{ $ongoing->duration }}</span>{{ $ongoing->end }}</span></h4>
                    </a>
                  </div>
                </div>
              @empty
                <div class="alert alert-danger text-center">
                    <span><b>Não existem agendamentos em andamento</b></span>
                </div>
              @endforelse
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card card-wizard wizard-card ct-wizard-orange" id="wizardCard">
          <div class="header">
              <h4 class="title">Serviços em espera</h4>
              <p class="category">Mantenha seus clientes atualizados</p>
          </div>
          <div class="content">
            <div class="row petshop-grid schedules-grid">
              @forelse($nextrow as $nextrow)
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                  <div class="col-md-12" style="padding: 0">
                    <a>
                      <div class="petshop-container schedule-container next-service bg-gray" style="background-image: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url({{ $nextrow->pet_image }})">
                        <div class="user-image back-image" style="background-image: url({{ $nextrow->pet_image }})" onclick="serviceDetails({{ $nextrow->schedule_id }})"></div>
                        <h3 onclick="serviceDetails({{ $nextrow->schedule_id }})">{{ $nextrow->service_name }}</h3>
                        <h4><span>Cliente</span> · {{ $nextrow->username }} <br><span>Pet</span> · {{ $nextrow->pet_name }}</h4>
                        <div class="action-status-btn start" onclick="updateServiceStatus('start', {{ $nextrow->schedule_id }}, {{ $petshopId }})">
                          <i class="pe-7s-clock white"></i>
                          <h6>Iniciar</h6>
                        </div>
                      </div>
                      <h4 class="time time-to-start bg-main-color"><i class="fa fa-clock-o white"></i>Início · <span>{{ $nextrow->date }}</span></h4>
                    </a>
                  </div>
                </div>
              @empty
                <div class="alert alert-danger text-center">
                    <span><b>Não existem agendamentos na fila para hoje</b></span>
                </div>
              @endforelse
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card card-wizard wizard-card ct-wizard-orange" id="wizardCard">
          <div class="header">
              <h4 class="title">Aguardando aprovação</h4>
              <p class="category">Mantenha seus clientes atualizados</p>
          </div>
          <div class="content">
            <div class="row petshop-grid schedules-grid">
              @forelse($waiting as $waiting)
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                  <div class="col-md-12" style="padding: 0">
                    <a>
                      <div class="petshop-container schedule-container waiting bg-gray" style="background-image: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url({{ $waiting->pet_image }})">
                        <div class="user-image back-image" style="background-image: url({{ $waiting->pet_image }})" onclick="serviceDetails({{ $waiting->schedule_id }})"></div>
                        <h3 onclick="serviceDetails({{ $waiting->schedule_id }})">{{ $waiting->service_name }}</h3>
                        <h4><span>Cliente</span> · {{ $waiting->username }} <br><span>Pet</span> · {{ $waiting->pet_name }}</h4>
                        <div class="action-status-btn aprove" onclick="updateServiceStatus('aprove', {{ $waiting->schedule_id }}, {{ $petshopId }})">
                          <i class="pe-7s-unlock white"></i>
                          <h6>Aprovar</h6>
                        </div>
                      </div>
                      <h4 class="time waiting-time bg-yellow"><i class="fa fa-clock-o white"></i><span>{{ $waiting->date }}</span></h4>
                    </a>
                  </div>
                </div>
              @empty
                <div class="alert alert-danger text-center">
                    <span><b>Não existem agendamentos aguardando aprovação</b></span>
                </div>
              @endforelse
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card card-wizard wizard-card ct-wizard-orange" id="wizardCard">
          <div class="header">
              <h4 class="title">Serviços finalizados</h4>
              <p class="category">Mantenha seus clientes atualizados</p>
          </div>
          <div class="content">
            <div class="row petshop-grid schedules-grid">
              @forelse($finished as $finished)
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                  <div class="col-md-12" style="padding: 0">
                    <a>
                      <div class="petshop-container schedule-container next-service bg-gray" style="background-image: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url({{ $finished->pet_image }})">
                        <div class="user-image back-image" style="background-image: url({{ $finished->pet_image }})" onclick="serviceDetails({{ $finished->schedule_id }})"></div>
                        <h3 onclick="serviceDetails({{ $finished->schedule_id }})">{{ $finished->service_name }}</h3>
                        <h4><span>Cliente</span> · {{ $finished->username }} <br><span>Pet</span> · {{ $finished->pet_name }}</h4>
                      </div>
                      <h4 class="time time-to-start bg-green"><i class="fa fa-clock-o white"></i>Concluído · <span>{{ $finished->date }}</span></h4>
                    </a>
                  </div>
                </div>
              @empty
                <div class="alert alert-danger text-center">
                    <span><b>Não existem agendamentos finalizados hoje</b></span>
                </div>
              @endforelse
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card card-wizard wizard-card ct-wizard-orange" id="wizardCard">
          <div class="header">
              <h4 class="title">Serviços cancelados</h4>
              <p class="category">Mantenha seus clientes atualizados</p>
          </div>
          <div class="content">
            <div class="row petshop-grid schedules-grid">
              @forelse($canceled as $canceled)
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                  <div class="col-md-12" style="padding: 0">
                    <a>
                      <div class="petshop-container schedule-container next-service bg-gray" style="background-image: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url({{ $canceled->pet_image }})">
                        <div class="user-image back-image" style="background-image: url({{ $canceled->pet_image }})" onclick="serviceDetails({{ $canceled->schedule_id }})"></div>
                        <h3 onclick="serviceDetails({{ $canceled->schedule_id }})">{{ $canceled->service_name }}</h3>
                        <h4><span>Cliente</span> · {{ $canceled->username }} <br><span>Pet</span> · {{ $canceled->pet_name }}</h4>
                      </div>
                      <h4 class="time time-to-start bg-red"><i class="fa fa-ban white"></i>Cancelado</h4>
                    </a>
                  </div>
                </div>
              @empty
                <div class="alert alert-danger text-center">
                    <span><b>Não existem agendamentos cancelados hoje</b></span>
                </div>
              @endforelse
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
