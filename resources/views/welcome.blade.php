<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Agende os serviços que você precisa para o seu Pet diretamennte do seu Smartphone">
    <meta name="author" content="LinkedPet">
    <title>LinkedPet</title>

    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico">

    <!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
	  <link href="css/estrutura.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body id="home" class="homepage">

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand logo" href="http://www.linkedpet.com.br"></a>
                </div>

                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="scroll active"><a href="#home">Home</a></li>
                        <li class="scroll"><a href="#features">Tenho um Pet</a></li>
                        <li class="scroll"><a href="#portfolio">Tenho um Pet Shop</a></li>
                        <li class="scroll"><a href="#about">Quem Somos</a></li>
                        <li class="scroll"><a href="#get-in-touch">Contato</a></li>
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->

    <section id="main-slider">
        <div class="">
            <div class="item" style="background-image: url(images/cachorro.jpg);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
							<div class="col-sm-3"></div>
                            <div class="col-sm-6 container-text text-center">
                                <div class="carousel-content">
                                    <h2><span>Conecte-se</span> com os melhores <span class="orange">serviços</span> disponíveis para o seu <span>pet</span></h2>
                                </div>
                            </div>
							<div class="col-sm-3"></div>
                        </div>
                    </div>
                </div>
            </div><!--/.item-->
            <!--<div class="item" style="background-image: url(images/slider/bg2.jpg);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h2>Beautifully designed <span>free</span> one page template</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et  dolore magna incididunt ut labore aliqua. </p>
                                    <a class="btn btn-primary btn-lg" href="#">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--><!--/.item-->
        </div><!--/.owl-carousel-->
    </section><!--/#main-slider-->

    <section id="cta" class="wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2>Por que utilizar o <b>LinkedPet</b>?</h2>
                    <p>Encontre os melhores Pet Shops perto de você, agende o serviço
                      ideal para seu pet de forma rápida e prática e ainda seja notificado
                      diretamente no seu smartphone sobre todo o status de realização do serviço.
                      Além disso, você ainda poderá avaliar o petshop e o serviço utilizado logo
                      após seu término!
                    </p>
                    <a class="link-button">Encontre Aqui ></a>
                </div>
            </div>
        </div>
    </section><!--/#cta-->

    <section id="features">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Tenho um Pet</h2>
                <p class="text-center wow fadeInDown">Os melhores serviços acessíveis diretamente do seu smartphone</p>
            </div>
            <div class="row">
                <div class="col-sm-6 wow fadeInLeft">
                    <img class="img-responsive" src="images/pet.png" alt="" style="margin-top: 6rem; width: 100%;">
                </div>
                <div class="col-sm-6">
                    <div class="media service-box wow fadeInRight">
                        <div class="pull-left">
                            <i class="fa fa-paw"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Encontre o que precisa</h4>
                            <p>O melhor e mais próximo Pet Shop de você, com avaliações de quem já utilizou e aprovou</p>
                        </div>
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="pull-left">
                            <i class="fa fa-stethoscope"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Facilidade e Praticidade</h4>
                            <p>Faça o agendamento online do serviço desejado recebendo um lembrete automático do compromisso e tudo isso através do seu Smartphone</p>
                        </div>
                    </div>

                    <!-- <div class="media service-box wow fadeInRight">
                        <div class="pull-left">
                            <i class="fa fa-medkit"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Ajude e economize</h4>
                            <p>Avalie o serviço utilizado para que outras pessoas possam sempre levar seus animais nos melhores Pet Shops e ganhe cupons de desconto</p>
                        </div>
                    </div> -->

                    <div class="media service-box wow fadeInRight">
                        <div class="pull-left">
                            <i class="fa fa-heart"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Aprenda com seu veterinário</h4>
                            <p>Receba em seu smartphone dicas e sugestões do seu veterinário sobre como cuidar ainda melhor de seu Pet e deixá-lo ainda mais feliz e saudável</p>
                        </div>
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="pull-left">
                            <div class="social-buttons pull-left"><a class="google-play left pull-left" href="https://play.google.com/store/apps/details?id=com.raulvbrito.linkedpet"></a><a class="app-store left pull-left" href="https://itunes.apple.com/br/app/linkedpet/id1099627118?mt=8"></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cta2">
        <div class="container">
            <div class="row text-center">
              <div class="col-sm-12 text-center">
                <h2 class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="0ms">E se eu tiver um <b>Pet Shop</b>, como faço para divulgá-lo?</h2>
                <p class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="100ms">
                  Inscreva-se no LinkedPet gratuitamente, aumente sua base de clientes e ainda
                  otimize a gestão dos serviços oferecidos pelo seu negócio em tempo real. Com
                  o LinkedPet, seu cliente poderá a qualquer momento agendar os serviços do seu
                  Pet Shop de acordo com o disponibilidade de horário de cada um de seus
                  respectivos serviços. Além disso, você ainda pode manter seu cliente informado
                  do status de cada etapa da realização do serviço solicitado, mantendo assim seu
                  cliente sempre perto de você e fidelizando-o de forma que agregue valor ao
                  serviço realizado através da agilidade e praticidade proporcionada pelo uso de
                  nossa ferramenta, gerando assim grande visibilidade ao seu negócio!</p>
                <!-- <img class="img-responsive wow fadeIn" src="images/cta2/cta2-img.png" alt="" data-wow-duration="300ms" data-wow-delay="300ms"> -->
              </div>
        </div>
    </section>

    <section id="portfolio" style="padding-bottom: 0">
      <div class="container">
          <div class="section-header">
              <h2 class="section-title text-center wow fadeInDown"> Sou Dono de Pet Shop</h2>
              <p class="text-center wow fadeInDown">Seja ainda mais notado no mercado oferecendo seus serviços de forma mais prática e inteligente através do LinkedPet</p>
          </div>
          <div class="row">
              <div class="col-sm-6 wow fadeInLeft">
                  <img class="img-responsive" src="images/veterinarian.jpg" alt="" style="margin-top: -1rem; margin-left: -17rem; max-width: 123%;">
              </div>
              <div class="col-sm-6">
                  <div class="media service-box wow fadeInRight">
                      <div class="pull-left">
                          <i class="fa fa-paw"></i>
                      </div>
                      <div class="media-body">
                          <h4 class="media-heading">Facilite a vida de seu cliente</h4>
                          <p>Proporcione um sistema de agendamento online e 24H e avise ao dono quando o Pet já está pronto para voltar para casa</p>
                      </div>
                  </div>

                  <div class="media service-box wow fadeInRight">
                      <div class="pull-left">
                          <i class="fa fa-stethoscope"></i>
                      </div>
                      <div class="media-body">
                          <h4 class="media-heading">Fidelize seu cliente</h4>
                          <p>Fidelize seus clientes tendo um canal de comunicação ágil e prático mantendo seu cliente sempre perto de você</p>
                      </div>
                  </div>

                  <div class="media service-box wow fadeInRight">
                      <div class="pull-left">
                          <i class="fa fa-medkit"></i>
                      </div>
                      <div class="media-body">
                          <h4 class="media-heading">Divulgue suas promoções</h4>
                          <p>Aumente seu faturamento oferecendo promoções e produtos direcionadas a cada cliente através do aplicativo</p>
                      </div>
                  </div>

                  <div class="media service-box wow fadeInRight">
                      <div class="pull-left">
                          <i class="fa fa-heart"></i>
                      </div>
                      <div class="media-body">
                          <h4 class="media-heading">Ganhe visibilidade</h4>
                          <p>Aqui, você poderá ser visto por todos usuários do LinkedPet, já imaginou quantos clientes você pode alcançar?</p>
                      </div>
                  </div>

                  <div class="media service-box wow fadeInRight">
                      <div class="pull-left">
                          <i class="fa fa-heart"></i>
                      </div>
                      <div class="media-body">
                          <h4 class="media-heading">Seja reconhecido pelo mercado</h4>
                          <p>Tenha seu selo de qualidade, conquistado com seu suor e competência, através de avaliação de quem já utilizou seus serviços e produtos</p>
                      </div>
                  </div>
                  <div class="media service-box wow fadeInRight">
                    <a class="link-button" href="http://www.linkedpet.com.br/auth/register">Cadastre-se Aqui ></a>
                  </div>
              </div>
          </div>
      </div>
    </section><!--/#portfolio-->

    <section id="work-process">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Como Funciona</h2>
                <p class="text-center wow fadeInDown">Descubra as vantagens e benefícios de utilizar o LinkedPet</p>
            </div>

            <div class="row text-center">
                <div class="col-md-2 col-md-4 col-xs-6">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="icon-circle">
                            <span>1</span>
                            <i class="fa fa-coffee fa-2x"></i>
                        </div>
                        <h3>BAIXE O APP</h3>
                    </div>
                </div>
                <div class="col-md-2 col-md-4 col-xs-6">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
                        <div class="icon-circle">
                            <span>2</span>
                            <i class="fa fa-bullhorn fa-2x"></i>
                        </div>
                        <h3>CADASTRE-SE</h3>
                    </div>
                </div>
                <div class="col-md-2 col-md-4 col-xs-6">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">
                        <div class="icon-circle">
                            <span>3</span>
                            <i class="fa fa-image fa-2x"></i>
                        </div>
                        <h3>PROCURE POR PET SHOPS PRÓXIMOS</h3>
                    </div>
                </div>
                <div class="col-md-2 col-md-4 col-xs-6">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="300ms">
                        <div class="icon-circle">
                            <span>4</span>
                            <i class="fa fa-heart fa-2x"></i>
                        </div>
                        <h3>ESCOLHA SEU SERVIÇO</h3>
                    </div>
                </div>
                <div class="col-md-2 col-md-4 col-xs-6">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="400ms">
                        <div class="icon-circle">
                            <span>5</span>
                            <i class="fa fa-shopping-cart fa-2x"></i>
                        </div>
                        <h3>AGENDE</h3>
                    </div>
                </div>
                <div class="col-md-2 col-md-4 col-xs-6">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="500ms">
                        <div class="icon-circle">
                            <span>6</span>
                            <i class="fa fa-space-shuttle fa-2x"></i>
                        </div>
                        <h3>:)</h3>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#work-process-->

    <section id="about">
        <div class="container">

            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Quem somos</h2>
                <p class="text-center wow fadeInDown">
                  O LinkedPet nasceu para otimizar a vida dos Pet Shops e de seus clientes, descomplicando tarefas antes muito burocráticas e tornando seu dia a dia muito mais prático. Através de uma solução multiplataforma, o cliente poderá realizar agendamentos dos serviços desejados via aplicativo através de seu smartphone Android ou iOS. Além disso, proporciona ao Pet Shop um painel completo contendo todas as informações necessárias sobre seus agendamentos e ainda possui um canal ágil de comunicação com o cliente. E sabe quem ficará ainda mais feliz com tudo isso? O seu animalzinho, que será tratado da melhor forma possível e receberá um serviço de ótima qualidade.
                </p>
            </div>

        </div>
    </section><!--/#about-->

    <!-- <section id="animated-number">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">E nesse momento nós somos...</h2>
            </div>

            <div class="row text-center">
                <div class="col-sm-3 col-xs-6">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="animated-number" data-digit="2305" data-duration="1000"></div>
                        <strong>PETS FELIZES COM UM SERVIÇO DE QUALIDADE</strong>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
                        <div class="animated-number" data-digit="1231" data-duration="1000"></div>
                        <strong>PET SHOPS CADASTRADOS</strong>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">
                        <div class="animated-number" data-digit="3025" data-duration="1000"></div>
                        <strong>USUÁRIOS CADASTRADOS</strong>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="300ms">
                        <div class="animated-number" data-digit="1199" data-duration="1000"></div>
                        <strong>PET SHOPS COM AVALIAÇÃO GERAL ACIMA DE 4 ESTRELAS</strong>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <!-- <section id="pricing">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Pricing Table</h2>
                <p class="text-center wow fadeInDown">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
            </div>

            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="wow zoomIn" data-wow-duration="400ms" data-wow-delay="0ms">
                        <ul class="pricing">
                            <li class="plan-header">
                                <div class="price-duration">
                                    <span class="price">
                                        $39
                                    </span>
                                    <span class="duration">
                                        per month
                                    </span>
                                </div>

                                <div class="plan-name">
                                    Starter
                                </div>
                            </li>
                            <li><strong>1</strong> DOMAIN</li>
                            <li><strong>100GB</strong> DISK SPACE</li>
                            <li><strong>UNLIMITED</strong> BANDWIDTH</li>
                            <li>SHARED SSL CERTIFICATE</li>
                            <li><strong>10</strong> EMAIL ADDRESS</li>
                            <li><strong>24/7</strong> SUPPORT</li>
                            <li class="plan-purchase"><a class="btn btn-primary" href="#">ORDER NOW</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="wow zoomIn" data-wow-duration="400ms" data-wow-delay="200ms">
                        <ul class="pricing featured">
                            <li class="plan-header">
                                <div class="price-duration">
                                    <span class="price">
                                        $69
                                    </span>
                                    <span class="duration">
                                        per month
                                    </span>
                                </div>

                                <div class="plan-name">
                                    Business
                                </div>
                            </li>
                            <li><strong>3</strong> DOMAIN</li>
                            <li><strong>300GB</strong> DISK SPACE</li>
                            <li><strong>UNLIMITED</strong> BANDWIDTH</li>
                            <li>SHARED SSL CERTIFICATE</li>
                            <li><strong>30</strong> EMAIL ADDRESS</li>
                            <li><strong>24/7</strong> SUPPORT</li>
                            <li class="plan-purchase"><a class="btn btn-default" href="#">ORDER NOW</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="wow zoomIn" data-wow-duration="400ms" data-wow-delay="400ms">
                        <ul class="pricing">
                            <li class="plan-header">
                                <div class="price-duration">
                                    <span class="price">
                                        $99
                                    </span>
                                    <span class="duration">
                                        per month
                                    </span>
                                </div>

                                <div class="plan-name">
                                    Pro
                                </div>
                            </li>
                            <li><strong>5</strong> DOMAIN</li>
                            <li><strong>500GB</strong> DISK SPACE</li>
                            <li><strong>UNLIMITED</strong> BANDWIDTH</li>
                            <li>SHARED SSL CERTIFICATE</li>
                            <li><strong>50</strong> EMAIL ADDRESS</li>
                            <li><strong>24/7</strong> SUPPORT</li>
                            <li class="plan-purchase"><a class="btn btn-primary" href="#">ORDER NOW</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="wow zoomIn" data-wow-duration="400ms" data-wow-delay="600ms">
                        <ul class="pricing">
                            <li class="plan-header">
                                <div class="price-duration">
                                    <span class="price">
                                        $199
                                    </span>
                                    <span class="duration">
                                        per month
                                    </span>
                                </div>

                                <div class="plan-name">
                                    Ultra
                                </div>
                            </li>
                            <li><strong>10</strong> DOMAIN</li>
                            <li><strong>1000GB</strong> DISK SPACE</li>
                            <li><strong>UNLIMITED</strong> BANDWIDTH</li>
                            <li>SHARED SSL CERTIFICATE</li>
                            <li><strong>100</strong> EMAIL ADDRESS</li>
                            <li><strong>24/7</strong> SUPPORT</li>
                            <li class="plan-purchase"><a class="btn btn-primary" href="#">ORDER NOW</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <section id="get-in-touch">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Experimente o Linked Pet</h2>
                <p class="text-center wow fadeInDown">Faça seu cadastro e ganhe desconto em seu primeiro agendamento</p>
            </div>
        </div>
    </section><!--/#get-in-touch-->


    <section id="contact">
        <div id="google-map" style="height:650px" data-latitude="-26.2872958" data-longitude="-48.8745385"></div>
        <div class="container-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-8">
                        <div class="contact-form">
                            <h3>Contato</h3>

                            <address>
                              <strong>LinkedPet</strong><br>
                              Rua Marechal Hermes, 1050 - 89217-200<br>
                              Joinville, SC - Brasil<br>
                              <abbr title="Phone">Tel:</abbr> (47) 9988-4469
                            </address>

                            <form id="main-contact-form" name="contact-form">
                                <div class="form-group">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Nome" required>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="subject" id="subject" class="form-control" placeholder="Assunto" required>
                                </div>
                                <div class="form-group">
                                    <textarea name="message" id="message" class="form-control" rows="8" placeholder="Mensagem" required></textarea>
                                </div>
                                <button class="btn btn-primary" onclick="sendContactEmail()">Enviar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#bottom-->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2016 LinkedPet. Desenvolvido por <a target="_blank">Raul Brito</a>
                </div>
                <div class="col-sm-6">
                    <ul class="social-icons">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a href="#"><i class="fa fa-behance"></i></a></li>
                        <li><a href="#"><i class="fa fa-flickr"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-github"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mousescroll.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/welcome.js"></script>
    <script src="js/main.js"></script>
</body>
</html>
