@extends('initial')

@section('content')
<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand logo" href="http://www.linkedpet.com.br"></a>
        </div>
        <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav navbar-right">
                <li>
                   <a href="register.html">
                        Cadastre-se
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="wrapper wrapper-full-page">
    <div class="full-page login-page" data-color="orange">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <form role="form" method="POST" action="{{ url('/auth/register') }}">
                            <div class="card">
                                <div class="header text-center">Cadastro</div>
                                <div class="content">
                                    @if (count($errors) > 0)
                                      <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                    @endif
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="role" value="owner">
                                    <div class="form-group">
                                        <label>Nome</label>
                                        <input type="text" name="name" class="form-control" placeholder="Ex: João Silva" value="{{ old('name') }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" class="form-control" placeholder="Ex: joao.silva@gmail.com" value="{{ old('email') }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Senha</label>
                                        <input type="password" name="password" id="password" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Confirme sua senha</label>
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <button type="submit" class="btn btn-fill btn-warning btn-wd">Salvar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    	<footer class="footer footer-transparent">
            <div class="container">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="#">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                A Empresa
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    © 2016 <a href="http://www.linkedpet.com.br">LinkedPet</a>
                </p>
            </div>
        </footer>

    <div class="full-page-background" style="background-image: url(../../assets/img/full-screen-image-1.jpg) "></div></div>

</div>
<!-- <div class="container-fluid">
    <div class="row">
      <div class="col-md-2 login-content">
          <h1 class="title text-center white"><b>LINKED</b>PET</h1>
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
          <div class="card">
							<form role="form" method="POST" action="{{ url('/auth/register') }}">
              	<div class="content">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="role" value="owner">
                  <input type="text" name="name" class="form-control" placeholder="Nome" value="{{ old('name') }}">
                  <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
                  <input type="password" name="password" id="password" class="form-control" placeholder="Senha">
                  <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirme sua senha">
								</div>
                <button class="btn bg-second-color btn-fill pull-right" type="submit">Cadastrar</button>
							</form>
          </div>
      </div>
    </div>
</div> -->
@endsection
