<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'O campo :attribute must be accepted.',
    'active_url'           => 'O campo :attribute is not a valid URL.',
    'after'                => 'O campo :attribute must be a date after :date.',
    'alpha'                => 'O campo :attribute may only contain letters.',
    'alpha_dash'           => 'O campo :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'O campo :attribute may only contain letters and numbers.',
    'array'                => 'O campo :attribute must be an array.',
    'before'               => 'O campo :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'O campo :attribute must be between :min and :max.',
        'file'    => 'O campo :attribute must be between :min and :max kilobytes.',
        'string'  => 'O campo :attribute must be between :min and :max characters.',
        'array'   => 'O campo :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'O campo :attribute field must be true or false.',
    'confirmed'            => 'As senhas devem ser iguais.',
    'date'                 => 'O campo :attribute is not a valid date.',
    'date_format'          => 'O campo :attribute does not match the format :format.',
    'different'            => 'O campo :attribute and :other must be different.',
    'digits'               => 'O campo :attribute must be :digits digits.',
    'digits_between'       => 'O campo :attribute must be between :min and :max digits.',
    'email'                => 'O campo :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'filled'               => 'O campo :attribute é obrigatório.',
    'image'                => 'O campo :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'integer'              => 'O campo :attribute must be an integer.',
    'ip'                   => 'O campo :attribute must be a valid IP address.',
    'json'                 => 'O campo :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'O campo :attribute may not be greater than :max.',
        'file'    => 'O campo :attribute may not be greater than :max kilobytes.',
        'string'  => 'O campo :attribute may not be greater than :max characters.',
        'array'   => 'O campo :attribute may not have more than :max items.',
    ],
    'mimes'                => 'O campo :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'O campo :attribute deve ser pelo menos :min.',
        'file'    => 'O campo :attribute must be at least :min kilobytes.',
        'string'  => 'O campo :attribute deve conter pelo menos :min caracteres.',
        'array'   => 'O campo :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'O campo :attribute must be a number.',
    'regex'                => 'O campo :attribute format is invalid.',
    'required'             => 'O campo :attribute  é obrigatório.',
    'required_if'          => 'O campo :attribute  é obrigatório when :other is :value.',
    'required_unless'      => 'O campo :attribute é obrigatório unless :other is in :values.',
    'required_with'        => 'O campo :attribute é obrigatório when :values is present.',
    'required_with_all'    => 'O campo :attribute é obrigatório when :values is present.',
    'required_without'     => 'O campo :attribute é obrigatório when :values is not present.',
    'required_without_all' => 'O campo :attribute é obrigatório when none of :values are present.',
    'same'                 => 'O campo :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'O campo :attribute must be :size.',
        'file'    => 'O campo :attribute must be :size kilobytes.',
        'string'  => 'O campo :attribute must be :size characters.',
        'array'   => 'O campo :attribute must contain :size items.',
    ],
    'string'               => 'O campo :attribute must be a string.',
    'timezone'             => 'O campo :attribute must be a valid zone.',
    'unique'               => 'O campo :attribute has already been taken.',
    'url'                  => 'O campo :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
