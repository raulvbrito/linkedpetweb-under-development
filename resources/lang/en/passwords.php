<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A senha deve ter pelo menos 6 caracteres e ser igual à confirmação.',
    'reset' => 'Sua senha foi atualizada!',
    'sent' => 'Mandamos um link para troca de senha no seu email!',
    'token' => 'O token de troca de senha é inválido.',
    'user' => "Usuário não encontrado.",

];
